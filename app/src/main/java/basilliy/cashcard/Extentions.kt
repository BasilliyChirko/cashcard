package basilliy.cashcard

import java.util.*

fun Int.withSpaces(): String {
    var result = ""
    val value = if (this > 0) this else -this
    val array = value.toString().toCharArray()
    for (i in array.size - 1 downTo 0) {
        result = array[i] + result
        if ((array.size - i) % 3 == 0)
            result = "," + result
    }
    return if (this >= 0) result.removePrefix(",")
    else "- " + result.removePrefix(",")
}

fun Int.toStringWithSign(): String {
    return if (this > 0) "+ ${this.withSpaces()}" else this.withSpaces()
}

fun today(): Calendar {
    val calendar = Calendar.getInstance()
    calendar.set(Calendar.HOUR_OF_DAY, 0)
    calendar.set(Calendar.MINUTE, 0)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)
    return calendar
}

fun todayMillis(): Long {
    return today().timeInMillis
}

fun Float.format(digits: Int) = java.lang.String.format("%.${digits}f", this)