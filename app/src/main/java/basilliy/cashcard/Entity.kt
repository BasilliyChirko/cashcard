package basilliy.cashcard


data class Source(
        var id: Long = 0,
        var name: String = "",
        var currency: String = "UAH",
        var balance: Int = 0,
        var showStatistic: Boolean = false,
        var hideBalance: Boolean = false,
        var positionOnList: Int = Int.MAX_VALUE,

        var forTodayValue: Int? = null,
        var forTodayTime: Long? = null
) {

    fun getTodayDeviation(): Int {
        return if (todayMillis() == forTodayTime) forTodayValue ?: 0 else 0
    }

    val currencySymbol: String
        get() = when (currency) {
            "USD" -> App.getContext().resources.getString(R.string.symbol_usd)
            "EUR" -> App.getContext().resources.getString(R.string.symbol_eur)
            "UAH" -> App.getContext().resources.getString(R.string.symbol_uah)
            else -> ""
        }

}

data class Transaction(
        var id: Long = 0,
        var sourceId: Long = 0,
        var value: Int = 0,
        var time: Long = System.currentTimeMillis()
)
