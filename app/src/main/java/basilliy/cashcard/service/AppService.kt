package basilliy.cashcard.service

import android.annotation.SuppressLint
import basilliy.cashcard.Source
import basilliy.cashcard.Transaction
import basilliy.cashcard.database.*
import basilliy.cashcard.database.tables.SourceTable
import basilliy.cashcard.database.tables.TransactionTable
import basilliy.cashcard.module.components.RequestDialog
import basilliy.cashcard.todayMillis
import basilliyc.extensions.tryOrNull
import org.json.JSONArray
import org.json.JSONObject
import kotlin.concurrent.thread
import kotlin.math.roundToInt

fun Source.save(): Long {
    return AppService.save(this)
}

fun Transaction.save(): Long {
    return AppService.save(this)
}

object AppService {

    private val dayInMillis by lazy { 1000L * 60L * 60L * 24L }

    //------------------- Source -------------------

    fun save(source: Source): Long {
        return if (source.id != 0L) {
            Database.get().update(SourceTable.tableName, SourceTable.values(source), "${SourceTable.id} = ${source.id}")
            source.id
        } else {
            source.positionOnList = getMaxPosition() + 1
            Database.get().insert(SourceTable.tableName, SourceTable.values(source))
        }
    }

    private fun getMaxPosition(): Int {
        val cursor = Database.get().rawQuery("SELECT MAX(${SourceTable.positionOnList}) as position FROM ${SourceTable.tableName}", arrayOf())
        val maxPos = if (cursor.moveToFirst()) cursor.getInt("position") else 0
        cursor.close()
        return maxPos
    }

    fun delete(source: Source) {
        Database.get().delete(SourceTable.tableName, "${SourceTable.id} = ${source.id}")
        Database.get().delete(TransactionTable.tableName, "${TransactionTable.sourceId} = ${source.id}")
    }

    @SuppressLint("Recycle")
    fun getSource(id: Long): Source? {
        return Database.get().query(SourceTable.tableName, "${SourceTable.id} = $id").readOne { SourceTable.read(it) }
    }

    fun getSourceAll(): ArrayList<Source> {
        val list = Database.get().query(SourceTable.tableName).readMany { SourceTable.read(it) }
        list.sortBy { it.positionOnList }
        return list
    }

    //------------------- Transaction -------------------

    fun save(transaction: Transaction): Long {
        return if (transaction.id != 0L) {
            Database.get().update(TransactionTable.tableName, TransactionTable.values(transaction), "${TransactionTable.id} = ${transaction.id}")
            transaction.id
        } else {
            Database.get().insert(TransactionTable.tableName, TransactionTable.values(transaction))
        }
    }

    fun delete(transaction: Transaction) {
        Database.get().delete(TransactionTable.tableName, "${TransactionTable.id} = ${transaction.id}")
    }

    @SuppressLint("Recycle")
    fun getTransaction(id: Long): Transaction? {
        return Database.get().query(TransactionTable.tableName, "${TransactionTable.id} = $id").readOne { TransactionTable.read(it) }
    }

    fun getAsyncTransactionSumForTodayBySourceId(sourceId: Long, callback: (Int) -> Unit) {
        val cursor = Database.get().rawQuery("select sum(value) as value from ${TransactionTable.tableName} where ${TransactionTable.sourceId} = $sourceId and time > ${todayMillis()}", null)

        if (cursor.moveToFirst()) {
            val value = cursor.getInt("value")
            cursor.close()
            callback(value)
        } else {
            cursor.close()
            callback(0)
        }
    }

    fun getTransactionBySourceIdForToday(sourceId: Long): ArrayList<Transaction> {
        return Database.get().query(TransactionTable.tableName, null, "${TransactionTable.sourceId} = $sourceId and ${TransactionTable.time} >= ${todayMillis()}", null, null, null, TransactionTable.time + " desc", null).readMany { TransactionTable.read(it) }
    }

    fun getTransactionBySourceId(sourceId: Long, time: Long = System.currentTimeMillis(), limit: Int = 15): ArrayList<Transaction> {
        return Database.get().query(TransactionTable.tableName, null, "${TransactionTable.sourceId} = $sourceId and ${TransactionTable.time} < $time", null, null, null, TransactionTable.time + " desc", limit.toString()).readMany { TransactionTable.read(it) }
    }

    fun getTransactionBySourceId(sourceId: Long): ArrayList<Transaction> {
        return Database.get().query(TransactionTable.tableName, null, "${TransactionTable.sourceId} = $sourceId", null, null, null, TransactionTable.time + " desc", null).readMany { TransactionTable.read(it) }
    }

    fun getTransactionAll(): ArrayList<Transaction> {
        return Database.get().query(TransactionTable.tableName).readMany { TransactionTable.read(it) }
    }

    //------------------- Deviation -------------------

    fun addDeviation(source: Source, deviation: Int, callback: () -> Unit) {
        source.balance += deviation

        val todayMillis = todayMillis()

        if (source.forTodayTime != todayMillis)
            source.forTodayValue = 0

        source.forTodayValue = (source.forTodayValue ?: 0) + deviation
        source.forTodayTime = todayMillis
        source.save()

        Transaction(sourceId = source.id, value = deviation).save()

        callback()
    }


    //------------------- Export \ Import -------------------


    fun createDataJSON(): JSONObject {
        val result = JSONObject()

        val sources = JSONArray()
        getSourceAll().forEach { source ->
            val jsonSource = JSONObject()
            jsonSource.put(SourceTable.name, source.name)
            jsonSource.put(SourceTable.currency, source.currency)
            jsonSource.put(SourceTable.balance, source.balance)
            jsonSource.put(SourceTable.showOnStatistic, source.showStatistic)
            jsonSource.put(SourceTable.hideBalance, source.hideBalance)
            jsonSource.put(SourceTable.forTodayValue, source.forTodayValue)
            jsonSource.put(SourceTable.forTodayTime, source.forTodayTime)
            jsonSource.put(SourceTable.positionOnList, source.positionOnList)


            val transactions = JSONArray()
            getTransactionBySourceId(source.id).forEach { transition ->
                val jsonTransaction = JSONObject()
                jsonTransaction.put(TransactionTable.value, transition.value)
                jsonTransaction.put(TransactionTable.time, transition.time)
                transactions.put(jsonTransaction)
            }
            jsonSource.put(TransactionTable.tableName, transactions)

            sources.put(jsonSource)
        }
        result.put(SourceTable.tableName, sources)

        return result
    }

    fun readDataJSON(input: JSONObject, callback: () -> Unit) {
        thread {

            val sources = input.getJSONArray(SourceTable.tableName)

            val text = "Импорт "
            var countAll = sources.length()
            var countProgress = 0

            fun show() {
                countProgress++
                RequestDialog.nextTitle = text + " " + (countProgress.toFloat() / countAll.toFloat() * 100).roundToInt() + "%"
            }

            for (i in 0 until sources.length()) {
                countAll += sources.getJSONObject(i).getJSONArray(TransactionTable.tableName).length()
            }

            //--

            for (i in 0 until sources.length()) {
                val jSource = sources.getJSONObject(i)
                val source = Source()

                source.name = jSource.getString(SourceTable.name)
                source.currency = jSource.getString(SourceTable.currency)
                source.balance = jSource.getInt(SourceTable.balance)
                source.showStatistic = jSource.getBoolean(SourceTable.showOnStatistic)
                source.hideBalance = jSource.getBoolean(SourceTable.hideBalance)
                source.forTodayValue = jSource.getInt(SourceTable.forTodayValue)
                source.forTodayTime = jSource.getLong(SourceTable.forTodayTime)
                source.positionOnList = tryOrNull { jSource.getInt(SourceTable.positionOnList) } ?: 0

                val sourceId = source.save()
                show()

                val transactions = jSource.getJSONArray(TransactionTable.tableName)

                for (j in 0 until transactions.length()) {
                    val jTransaction = transactions.getJSONObject(j)
                    val transaction = Transaction()

                    transaction.sourceId = sourceId
                    transaction.value = jTransaction.getInt(TransactionTable.value)
                    transaction.time = jTransaction.getLong(TransactionTable.time)

                    transaction.save()
                    show()
                }

            }

            callback()

        }

    }

}