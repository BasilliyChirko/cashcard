package basilliy.cashcard.service

import basilliy.cashcard.server.CurrencyRepository
import dagger.Component
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

@Component(modules = [CurrencyRetrofitModule::class])
interface CurrencyRepositoryComponent {
    fun getRepository(): CurrencyRepository
}

@Module
class CurrencyRetrofitModule {

    @Provides
    fun getRepository(retrofit: Retrofit): CurrencyRepository {
        return retrofit.create(CurrencyRepository::class.java)
    }

    @Provides
    fun getRetrofit(factory: Converter.Factory): Retrofit {
        return Retrofit.Builder()
                .baseUrl("http://freecurrencyrates.com/ru/")
                .addConverterFactory(factory)
                .build()
    }

    @Provides
    fun converterFactory(): Converter.Factory {
        return ScalarsConverterFactory.create()
    }

    @Provides
    fun okhttp(interceptor: HttpLoggingInterceptor): OkHttpClient{
        val httpClient = OkHttpClient.Builder()
        httpClient.readTimeout(30L, TimeUnit.SECONDS)
        httpClient.connectTimeout(30L, TimeUnit.SECONDS)
        httpClient.addInterceptor(interceptor)
        return httpClient.build()
    }

    @Provides
    fun httpLogInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

}
