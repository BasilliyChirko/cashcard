package basilliy.cashcard.service

import basilliy.cashcard.Preferences
import basilliy.cashcard.server.CurrencyRepository
import basilliy.cashcard.today
import basilliyc.extensions.toCalendar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object CurrencyService {

//    private val repository: CurrencyRepository by lazy {
//        Retrofit.Builder()
//                .baseUrl("http://freecurrencyrates.com/ru/")
//                .addConverterFactory(ScalarsConverterFactory.create())
//                .build()
//                .create(CurrencyRepository::class.java)
//    }

    
    private val repository: CurrencyRepository = DaggerCurrencyRepositoryComponent.create().getRepository()

    fun getUSD(callback: ((Float) -> Unit)?) {
        val instance = today()
        if (Preferences.USDtoUAHUpdateTimestamp.toCalendar().before(instance)) {
            Preferences.USDtoUAHUpdateTimestamp = instance.timeInMillis

            repository.getPageUSDtoUAH().enqueue(object : Callback<String> {
                override fun onFailure(call: Call<String>?, t: Throwable?) {}

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    if (response.isSuccessful) {
                        try {
                            response.body()?.let {
                                val search = "Текущий курс обмена USD/UAH - "
                                val startIndex = it.indexOf(search)
                                val s = it.substring(startIndex + search.length, startIndex + search.length + 4)
                                Preferences.USDtoUAH = s.toFloat()
                            }
                        } catch (e: Exception) {

                        }
                    }
                    callback?.invoke(Preferences.USDtoUAH)
                }
            })
        } else callback?.invoke(Preferences.USDtoUAH)
    }

    fun getEUR(callback: ((Float) -> Unit)?) {
        val instance = today()
        if (Preferences.EURtoUAHUpdateTimestamp.toCalendar().before(instance)) {
            Preferences.EURtoUAHUpdateTimestamp = instance.timeInMillis

            repository.getPageEURtoUAH().enqueue(object : Callback<String> {
                override fun onFailure(call: Call<String>?, t: Throwable?) {}

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    if (response.isSuccessful) {
                        try {
                            response.body()?.let {
                                val search = "Текущий курс обмена EUR/UAH - "
                                val startIndex = it.indexOf(search)
                                val s = it.substring(startIndex + search.length, startIndex + search.length + 4)
                                Preferences.EURtoUAH = s.toFloat()
                            }
                        } catch (e: Exception) {

                        }
                    }
                    callback?.invoke(Preferences.EURtoUAH)
                }
            })
        } else callback?.invoke(Preferences.EURtoUAH)
    }

}