package basilliy.cashcard.server

import retrofit2.Call
import retrofit2.http.GET

interface CurrencyRepository {
    @GET("convert-USD-UAH")
    fun getPageUSDtoUAH(): Call<String>

    @GET("convert-EUR-UAH")
    fun getPageEURtoUAH(): Call<String>
}