package basilliy.cashcard

import basilliyc.extensions.secInMillis

object Constants {

    object Fragment {
        const val fragmentUseTime = secInMillis * 30
    }

}