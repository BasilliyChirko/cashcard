package basilliy.cashcard.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import basilliy.cashcard.App
import basilliy.cashcard.database.tables.SourceTable
import basilliy.cashcard.database.tables.TransactionTable

class Database(context: Context) : SQLiteOpenHelper(context, "Database", null, 4) {

    companion object {
        private var instance: Database? = null

        fun get(): SQLiteDatabase {
            if (instance == null) {
                instance = Database(App.getContext())
            }

            return instance!!.writableDatabase
        }

        fun close() {
            instance?.close()
            instance = null
        }

    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SourceTable.queryCreateTable())
        db.execSQL(TransactionTable.queryCreateTable())
    }

    @Suppress("UNUSED_VALUE")
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

        var currentVersion = oldVersion

        if (currentVersion < 2) {
            db.execSQL(TransactionTable.queryCreateTable())
            currentVersion = 2
        }

        if (currentVersion < 3) {
            db.execSQL("ALTER TABLE ${SourceTable.tableName} ADD COLUMN ${SourceTable.forTodayTime} integer;")
            db.execSQL("ALTER TABLE ${SourceTable.tableName} ADD COLUMN ${SourceTable.forTodayValue} integer;")
            currentVersion = 3
        }

        if (currentVersion < 4) {
            db.execSQL("ALTER TABLE ${SourceTable.tableName} ADD COLUMN ${SourceTable.positionOnList} integer DEFAULT 0")
            currentVersion = 4
        }

    }

}

fun SQLiteDatabase.transaction(call: (database: SQLiteDatabase) -> Unit, end: () -> Unit) {
    try {
        beginTransaction()
        call(this)
        setTransactionSuccessful()
    } finally {
        endTransaction()
        end()
    }
}

fun SQLiteDatabase.query(table: String): Cursor {
    return query(table, null, null, null, null, null, null, null)
}

fun SQLiteDatabase.query(table: String, where: String): Cursor {
    return query(table, null, where, null, null, null, null, null)
}

fun SQLiteDatabase.insert(table: String, values: ContentValues): Long {
    return insert(table, null, values)
}

fun SQLiteDatabase.update(table: String, values: ContentValues, where: String) {
    update(table, values, where, arrayOf())
}

fun SQLiteDatabase.delete(table: String, where: String) {
    delete(table, where, null)
}

fun Cursor.getInt(column: String): Int {
    return this.getInt(this.getColumnIndex(column))
}

fun Cursor.getLong(column: String): Long {
    return this.getLong(this.getColumnIndex(column))
}

fun Cursor.getString(column: String): String {
    return this.getString(this.getColumnIndex(column))
}

fun Cursor.getBoolean(column: String): Boolean {
    return this.getInt(this.getColumnIndex(column)) == 1
}

fun <T> Cursor.readOne(predicate: (Cursor) -> T): T? {
    val t = if (moveToFirst()) predicate(this) else null
    close()
    return t
}

fun <T> Cursor.readMany(predicate: (Cursor) -> T): ArrayList<T> {
    val list = ArrayList<T>()

    if (moveToFirst()) do {
        list.add(predicate(this))
    } while (moveToNext())

    close()

    return list
}