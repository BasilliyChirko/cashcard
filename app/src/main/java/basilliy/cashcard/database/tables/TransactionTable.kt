@file:Suppress("MemberVisibilityCanBePrivate")

package basilliy.cashcard.database.tables

import android.content.ContentValues
import android.database.Cursor
import basilliy.cashcard.Transaction
import basilliy.cashcard.database.getInt
import basilliy.cashcard.database.getLong

object TransactionTable {

    const val tableName = "Trans"

    const val id = "id"
    const val sourceId = "sourceId"
    const val value = "value"
    const val time = "time"

    fun queryCreateTable(): String {
        return "create table $tableName (\n" +
                "\t$id integer primary key autoincrement,\n" +
                "\t$sourceId integer,\n" +
                "\t$value integer,\n" +
                "\t$time integer\n" +
                ");"
    }

    fun values(transaction: Transaction): ContentValues = ContentValues().apply {
        put(sourceId, transaction.sourceId)
        put(value, transaction.value)
        put(time, transaction.time)
    }

    fun read(c: Cursor): Transaction {
        val transition = Transaction()

        transition.id = c.getLong(id)
        transition.sourceId = c.getLong(sourceId)
        transition.time = c.getLong(time)
        transition.value = c.getInt(value)

        return transition
    }


}