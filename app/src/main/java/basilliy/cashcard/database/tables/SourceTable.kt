@file:Suppress("MemberVisibilityCanBePrivate")

package basilliy.cashcard.database.tables

import android.content.ContentValues
import android.database.Cursor
import basilliy.cashcard.Source
import basilliy.cashcard.database.getBoolean
import basilliy.cashcard.database.getInt
import basilliy.cashcard.database.getLong
import basilliy.cashcard.database.getString

object SourceTable {

    const val tableName = "Source"

    const val id = "id"
    const val name = "name"
    const val currency = "currency"
    const val balance = "balance"
    const val showOnStatistic = "showOnStatistic"
    const val hideBalance = "hideBalance"
    const val positionOnList = "positionOnList"

    const val forTodayValue = "forTodayValue"
    const val forTodayTime = "forTodayTime"

    fun queryCreateTable(): String {
        return "create table $tableName (\n" +
                "\t$id integer primary key autoincrement,\n" +
                "\t$name text,\n" +
                "\t$currency text,\n" +
                "\t$balance integer,\n" +
                "\t$showOnStatistic integer,\n" +
                "\t$forTodayValue integer,\n" +
                "\t$forTodayTime integer,\n" +
                "\t$hideBalance integer,\n" +
                "\t$positionOnList integer DEFAULT 0\n" +
                ");"
    }

    fun values(source: Source): ContentValues = ContentValues().apply {
        put(name, source.name)
        put(currency, source.currency)
        put(balance, source.balance)
        put(showOnStatistic, if (source.showStatistic) 1 else 0)
        put(hideBalance, if (source.hideBalance) 1 else 0)
        put(positionOnList, source.positionOnList)

        put(forTodayValue, source.forTodayValue)
        put(forTodayTime, source.forTodayTime)
    }

    fun read(c: Cursor): Source {
        val source = Source()

        source.id = c.getLong(id)
        source.name = c.getString(name)
        source.currency = c.getString(currency)
        source.balance = c.getInt(balance)
        source.showStatistic = c.getBoolean(showOnStatistic)
        source.hideBalance = c.getBoolean(hideBalance)
        source.positionOnList = c.getInt(positionOnList)

        source.forTodayValue = c.getInt(forTodayValue)
        source.forTodayTime = c.getLong(forTodayTime)

        return source
    }


}