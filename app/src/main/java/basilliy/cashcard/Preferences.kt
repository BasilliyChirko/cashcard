@file:Suppress("UNCHECKED_CAST")

package basilliy.cashcard

import android.content.Context
import android.content.SharedPreferences
import basilliy.cashcard.module.navigation.PageType

object Preferences {

    private val pref: SharedPreferences by lazy {
        App.getContext().getSharedPreferences("preferences", Context.MODE_PRIVATE)
    }

    fun Any.save(key: String) {
        val edit = pref.edit()
        when (this) {
            is Int -> edit.putInt(key, this)
            is Long -> edit.putLong(key, this)
            is Float -> edit.putFloat(key, this)
            is Boolean -> edit.putBoolean(key, this)
            is String -> edit.putString(key, this)
        }
        edit.apply()
    }

    fun <T> load(key: String, default: T): T {
        return when (default) {
            is Int -> pref.getInt(key, default as Int) as T
            is Long -> pref.getLong(key, default as Long) as T
            is Float -> pref.getFloat(key, default as Float) as T
            is Boolean -> pref.getBoolean(key, default as Boolean) as T
            is String -> pref.getString(key, default as String) as T
            else -> "" as T
        }
    }

    var USDtoUAHUpdateTimestamp
        get() = load("USDUpdateTimestamp", 0L)
        set(value) = value.save("USDUpdateTimestamp")

    var USDtoUAH
        get() = load("USDtoUAH", 25f)
        set(value) = value.save("USDtoUAH")

    var EURtoUAHUpdateTimestamp
        get() = load("EURUpdateTimestamp", 0L)
        set(value) = value.save("EURUpdateTimestamp")

    var EURtoUAH
        get() = load("EURtoUAH", 25f)
        set(value) = value.save("EURtoUAH")

    var setup: Boolean
        get() = load("setup", false)
        set(value) = value.save("setup")

    var firstLogin: Boolean
        get() = load("firstLogin", true)
        set(value) = value.save("firstLogin")

    var navigatePage: PageType
        get() = PageType.valueOf(load("navigatePage", PageType.SOURCE.toString()))
        set(value) = value.toString().save("navigatePage")

    fun clean() {
        pref.edit().clear().apply()
    }

}