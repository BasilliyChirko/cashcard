@file:Suppress("DEPRECATION")

package basilliy.cashcard.module.components

import android.app.*
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import basilliy.cashcard.R
import basilliyc.extensions.anyTry
import kotlinx.android.synthetic.main.component_request_dialog.view.*

class RequestDialog : DialogFragment() {

    private var titleValue = ""
    private var itWasInitiated = false

    lateinit var titleView: TextView
    lateinit var errorIcon: ImageView

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val view = activity.layoutInflater.inflate(R.layout.component_request_dialog, null, false)

        val title = if (isNeedUseNextTitle()) nextTitle else arguments.getString(KEY_TITLE, "")
        if (title != "" && !itWasInitiated) {
            itWasInitiated = true
            titleValue = title
            titleView = view.request_dialog_title
            titleView.text = titleValue
            errorIcon = view.request_dialog_error_icon
        }

        EventBus.addListener(this::class.java) {
            if (it.action == EventBus.UPDATE) {
                view?.post { updateTitleAsNextAvailable() }
            }
        }

        val dialog = AlertDialog.Builder(activity!!, R.style.ThemeOverlay_AppCompat_Dialog)
                .setView(view)
                .create()

        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    override fun onDestroyView() {
        super.onDestroyView()
        EventBus.removeListener(this::class.java)
    }

    private fun isNeedUseNextTitle(): Boolean {
        return nextTitle != "" && (timeOnNextTitleSelected + timeWaitForNextTitle) > System.currentTimeMillis()
    }

    private fun updateTitleAsNextAvailable() {
        if (isNeedUseNextTitle()) {
            setTitle(nextTitle)
        }
    }

    fun setTitle(title: String) {
        titleValue = title
        titleView.text = titleValue
    }

    fun setError(title: String) {
        errorIcon.visibility = View.VISIBLE
        titleValue = title
        titleView.text = titleValue

        titleView.postDelayed({
            dismiss()
        }, 3000)
    }

    companion object {
        const val dialogTag = "RequestDialog"

        private const val KEY_TITLE = "KEY_TITLE"

        private var timeOnNextTitleSelected = 0L
        private const val timeWaitForNextTitle = 3000L
        var nextTitle = ""
            set(value) {
                timeOnNextTitleSelected = System.currentTimeMillis()
                field = value

                EventBus.sendMessage(RequestDialog::class.java, EventBus.UPDATE)
            }

        fun start(manager: FragmentManager, title: String): RequestDialog {
            val dialog = RequestDialog()
            val bundle = Bundle()
            bundle.putString(KEY_TITLE, title)
            dialog.arguments = bundle
            dialog.show(manager, dialogTag)
            return dialog
        }
    }


    object EventBus {

        data class Message(
                var action: String?,
                var data: Any? = null
        )

        private val listenerList = HashMap<Class<*>, (message: Message) -> Unit>()

        fun addListener(receiver: Class<*>, call: (message: Message) -> Unit) {
            listenerList[receiver] = call
        }

        fun removeListener(receiver: Class<*>) {
            listenerList.remove(receiver)
        }

        fun sendMessage(receiver: Class<*>, action: String? = null, data: Any? = null) {
            listenerList[receiver]?.invoke(Message(action = action, data = data))
        }

        const val UPDATE = "UPDATE"

    }

}

fun Activity.hideRequestDialog() {
    anyTry {
        val fragment = this.fragmentManager.findFragmentByTag(RequestDialog.dialogTag)
        if (fragment != null && fragment is RequestDialog) {
            fragment.dismiss()
        }
    }
}

fun Activity.setRequestDialogTitle(title: String) {
    anyTry {
        val fragment = this.fragmentManager.findFragmentByTag(RequestDialog.dialogTag)
        if (fragment != null && fragment is RequestDialog) {
            fragment.setTitle(title)
        }
    }
}

fun Activity.createRequestDialog(title: String = ""): RequestDialog {
    val fragment = this.fragmentManager.findFragmentByTag(RequestDialog.dialogTag)
    if (fragment != null && fragment is RequestDialog) {
        if (title.isNotEmpty())
            fragment.setTitle(title)
        return fragment
    }
    return RequestDialog.start(this.fragmentManager, title)
}




