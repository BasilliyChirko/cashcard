package basilliy.cashcard.module.components

import android.view.View
import basilliy.cashcard.R
import basilliyc.extensions.setTextColor
import basilliyc.extensions.visible
import kotlinx.android.synthetic.main.component_chip.view.*

class Chip(
        var view: View,
        var text: String
) {
    fun select(selected: Boolean) {

        val resources = view.context.resources
        view.chip_text.setTextColor(resource = if (selected) R.color.primaryDark else R.color.greyDark)
        view.chip_image.visible = selected
        view.background = resources.getDrawable(if (selected) R.drawable.background_chip_primary else R.drawable.background_chip_grey)
    }
}

class ChipsComponent(val chips: ArrayList<Chip>) {

    var onClick: (Chip) -> Unit = {}

    init {
        chips.forEachIndexed { indexFirst, c ->
            c.view.chip_text.text = c.text
            c.select(false)

            c.view.setOnClickListener {
                chips.forEachIndexed { indexSecond, chip ->
                    chip.select(indexFirst == indexSecond)
                }

                onClick(c)
            }
        }
    }

    fun setSelected(text: String) {
        val indexOfFirst = chips.indexOfFirst { it.text == text }

        chips.forEachIndexed { indexSecond, chip ->
            chip.select(indexOfFirst == indexSecond)
        }
    }


}