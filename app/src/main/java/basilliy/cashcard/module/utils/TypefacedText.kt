package basilliy.cashcard.module.utils

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.EditText
import android.widget.TextView
import basilliy.cashcard.App
import basilliy.cashcard.R
import com.google.android.material.textfield.TextInputLayout

class Font {
    companion object {

        val ROBOTO_BOLD_KEY = 1
        val ROBOTO_REGULAR_KEY = 4

        val ROBOTO_BOLD_FONT: Typeface by lazy {
            load(App.getContext(), "roboto_bold")
        }

        val ROBOTO_REGULAR_FONT: Typeface by lazy {
            load(App.getContext(), "roboto_regular")
        }

        fun font(key: Int): Typeface {
            return when (key) {
                ROBOTO_BOLD_KEY -> ROBOTO_BOLD_FONT
                ROBOTO_REGULAR_KEY -> ROBOTO_REGULAR_FONT
                else -> ROBOTO_REGULAR_FONT
            }
        }

        private fun load(context: Context, name: String): Typeface {
            return Typeface.createFromAsset(context.assets, "fonts/$name.ttf")
        }
    }
}

fun TextView.setTypeface(attrs: AttributeSet) {
    val array = context.obtainStyledAttributes(attrs, R.styleable.Font)
    typeface = Font.font(array.getInt(R.styleable.Font_font_style, Font.ROBOTO_REGULAR_KEY))
    array.recycle()
}

class EditTextTypeface(context: Context, attrs: AttributeSet) : EditText(context, attrs) {
    init {
        setTypeface(attrs)
    }
}

class TextViewTypeface(context: Context, attrs: AttributeSet) : TextView(context, attrs) {
    init {
        setTypeface(attrs)
    }
}

class TextInputLayoutTypeface(context: Context, attrs: AttributeSet) : TextInputLayout(context, attrs) {
    init {
        val array = context.obtainStyledAttributes(attrs, R.styleable.Font)
        typeface = Font.font(array.getInt(R.styleable.Font_font_style, Font.ROBOTO_REGULAR_KEY))
        array.recycle()
    }
}