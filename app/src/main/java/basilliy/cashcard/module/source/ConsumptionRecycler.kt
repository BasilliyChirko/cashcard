package basilliy.cashcard.module.source

import android.annotation.SuppressLint
import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import basilliy.cashcard.R
import basilliy.cashcard.Transaction
import basilliy.cashcard.service.AppService
import basilliy.cashcard.withSpaces
import basilliyc.extensions.formatTime
import basilliyc.extensions.toArrayList
import kotlinx.android.synthetic.main.consumption_element.view.*

class ConsumptionRecyclerHolder(val view: View) : RecyclerView.ViewHolder(view) {
    var value: TextView = view.consumption_value
    var date: TextView = view.consumption_date
}

class ConsumptionRecyclerAdapter(val activity: Activity, val sourceId: Long, val symbol: String, val onClick: (Transaction) -> Unit) : RecyclerView.Adapter<ConsumptionRecyclerHolder>() {

    var data: ArrayList<Transaction> = AppService.getTransactionBySourceId(sourceId).sortedByDescending { it.time }.toArrayList()

    var dateFormat = "dd MMMM HH:mm"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConsumptionRecyclerHolder {
        return ConsumptionRecyclerHolder(activity.layoutInflater.inflate(R.layout.consumption_element, parent, false))
    }

    override fun getItemCount(): Int = data.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ConsumptionRecyclerHolder, position: Int) {
        val item = data[position]

        holder.value.text = symbol + " " + item.value.withSpaces()
        holder.value.setTextColor(activity.resources.getColor(
                if (item.value > 0) R.color.primaryDark
                else R.color.accent
        )
        )

        holder.date.text = item.time.formatTime(dateFormat)

        holder.view.setOnClickListener { onClick(item) }

    }

    fun update() {
        data = AppService.getTransactionBySourceId(sourceId).sortedByDescending { it.time }.toArrayList()
        this.notifyDataSetChanged()
    }

}