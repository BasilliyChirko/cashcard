package basilliy.cashcard.module.source

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import basilliy.cashcard.R
import basilliy.cashcard.Source
import basilliy.cashcard.module.base.BaseActivity
import basilliy.cashcard.module.base.BaseFragment
import basilliy.cashcard.service.AppService
import basilliy.cashcard.withSpaces
import kotlinx.android.synthetic.main.source_history_activity.*

@SuppressLint("SetTextI18n")
class SourceHistoryActivity : BaseActivity() {

    companion object {
        const val KEY_SOURCE_ID = "KEY_SOURCE_ID"
    }

    lateinit var consumptionAdapter: ConsumptionRecyclerAdapter
    lateinit var source: Source

    var loading = false
    var canBeLoadMore = true

    private var currentFragmentSecond: BaseFragment? = null

    override fun setContentView(): Int = R.layout.source_history_activity

    override fun initUI() {
        super.initUI()
        supportActionBar?.hide()

        source = AppService.getSource(intent.getLongExtra(KEY_SOURCE_ID, -1L))!!

        consumptionAdapter = ConsumptionRecyclerAdapter(this, source.id, source.currencySymbol) {

            val fragment = TransactionFragment.newInstance(source.id, it.id)
            setSecondPage(fragment)
        }
        consumption_list.adapter = consumptionAdapter
        val linearLayoutManager = LinearLayoutManager(this)
        consumption_list.layoutManager = linearLayoutManager
        consumption_list.itemAnimator = DefaultItemAnimator()


        consumption_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if (canBeLoadMore) {

                    val totalItemCount = consumptionAdapter.data.lastIndex
                    val lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
                    if (!loading && totalItemCount == lastVisibleItem) {
                        loading = true
                        loadModeTransaction()
                    }
                }

            }
        })
    }

    override fun updateUI() {
        super.updateUI()
        updateSourceView()
        consumptionAdapter.update()
    }

    private fun updateSourceView() {
        source = AppService.getSource(source.id)!!
        source_name.text = source.name
        source_balance.text = source.currencySymbol + " " + source.balance.withSpaces()
        source_for_today_value.text = source.currencySymbol + " " + source.getTodayDeviation().withSpaces()
    }

    private fun loadModeTransaction() {
        val transaction = consumptionAdapter.data.lastOrNull()

        if (transaction != null) {
            val transactionBySourceId = AppService.getTransactionBySourceId(source.id, transaction.time)

            if (transactionBySourceId.isEmpty()) {
                canBeLoadMore = false
            } else {
                consumptionAdapter.data.addAll(transactionBySourceId)
                consumptionAdapter.data.sortByDescending { it.time }
                consumptionAdapter.notifyDataSetChanged()
            }

        } else {
            canBeLoadMore = false
        }

        loading = false
    }


    private fun setSecondPage(fragment: BaseFragment) {
        val transaction = supportFragmentManager.beginTransaction()

        currentFragmentSecond = fragment
        transaction.replace(R.id.navigation_content_second, fragment)

        transaction.commit()
    }

    fun removeSecondFragment() {
        if (currentFragmentSecond != null) {
            supportFragmentManager.beginTransaction().remove(currentFragmentSecond!!).commit()
            currentFragmentSecond = null
        }
    }

    override fun onBackPressed() {
        if (currentFragmentSecond != null) {
            currentFragmentSecond!!.onBackPressedExecute()
            return
        }

        super.onBackPressed()
    }

}
