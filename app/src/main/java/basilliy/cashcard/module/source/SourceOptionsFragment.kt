package basilliy.cashcard.module.source

import android.content.Intent
import android.os.Bundle
import basilliy.cashcard.R
import basilliy.cashcard.Source
import basilliy.cashcard.module.base.BaseSecondFragment
import basilliy.cashcard.module.base.EventBus
import basilliy.cashcard.module.base.ViewController
import basilliy.cashcard.module.navigation.NavigateActivity
import basilliy.cashcard.service.AppService
import kotlinx.android.synthetic.main.source_options_fragment.*

class SourceOptionsFragment : BaseSecondFragment() {

    companion object {

        private const val KEY_SOURCE_ID = "KEY_SOURCE_ID"

        fun newInstance(sourceId: Long = 0): SourceOptionsFragment {
            val fragment = SourceOptionsFragment()
            val bundle = Bundle()

            bundle.putLong(KEY_SOURCE_ID, sourceId)

            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var source: Source

    override fun setSecondContentView(): Int = R.layout.source_options_fragment

    override fun getUpdateStrategy(): ViewController.UpdateStrategy {
        return ViewController.UpdateStrategy.Always
    }

    override fun initValues() {
        super.initValues()

        val sourceId = arguments!!.getLong(KEY_SOURCE_ID)

        source = if (sourceId > 0) AppService.getSource(sourceId)!! else Source()

    }

    override fun initListeners() {
        super.initListeners()

        option_delete.setOnClickListener {
            AppService.delete(source)
            EventBus.update(SourceListFragment::class.java)
            close()
        }

        option_edit.setOnClickListener {
            (activity as? NavigateActivity)?.setSecondPage(SourceFormFragment.newInstance(sourceId = source.id))
        }

        option_history.setOnClickListener {
            (activity as? NavigateActivity)?.let {
                val intent = Intent(it, SourceHistoryActivity::class.java)
                intent.putExtra(SourceHistoryActivity.KEY_SOURCE_ID, source.id)
                startActivity(intent)
            }

            close(false)
        }

        option_move.setOnClickListener {
            val dialog = SourcePositionDialog()
            dialog.source = source
            dialog.show(activity?.supportFragmentManager, "tag")
            close()
        }

    }

}