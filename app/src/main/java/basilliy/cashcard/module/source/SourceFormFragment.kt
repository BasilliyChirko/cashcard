package basilliy.cashcard.module.source

import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.app.AppCompatDelegate
import basilliy.cashcard.R
import basilliy.cashcard.Source
import basilliy.cashcard.module.base.BaseActivity
import basilliy.cashcard.module.base.BaseSecondFragment
import basilliy.cashcard.module.base.EventBus
import basilliy.cashcard.module.base.ViewController
import basilliy.cashcard.module.components.Chip
import basilliy.cashcard.module.components.ChipsComponent
import basilliy.cashcard.service.AppService
import kotlinx.android.synthetic.main.source_form_fragment.*
import java.util.regex.Pattern

class SourceFormFragment : BaseSecondFragment() {

    companion object {

        private const val KEY_SOURCE_ID = "KEY_SOURCE_ID"

        fun newInstance(sourceId: Long = 0): SourceFormFragment {
            val fragment = SourceFormFragment()
            val bundle = Bundle()

            bundle.putLong(KEY_SOURCE_ID, sourceId)

            fragment.arguments = bundle
            return fragment
        }
    }

    private val regexpName: Pattern = Pattern.compile("([а-яА-ЯA-Za-z0-9\\s-]+)*")

    var currency = "UAH"
    var isClickable = true
    lateinit var currencyArray: ArrayList<String>
    lateinit var chipsComponent: ChipsComponent


    private lateinit var source: Source

    override fun setSecondContentView(): Int = R.layout.source_form_fragment

    override fun getUpdateStrategy(): ViewController.UpdateStrategy {
        return ViewController.UpdateStrategy.Always
    }

    override fun initValues() {
        super.initValues()

        val sourceId = arguments!!.getLong(KEY_SOURCE_ID)

        source = if (sourceId > 0) AppService.getSource(sourceId)!! else Source()

    }

    override fun initUI() {
        super.initUI()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        currencyArray = arrayListOf("UAH", "USD", "EUR")

        chipsComponent = ChipsComponent(arrayListOf(
                Chip(chip_uah, "UAH"),
                Chip(chip_usd, "USD"),
                Chip(chip_eur, "EUR")
        ))

        name.requestFocus()
        (activity as? BaseActivity)?.showKeyboard(name)

    }

    override fun initListeners() {
        super.initListeners()

        chipsComponent.onClick = { chip ->
            val first = currencyArray.indexOfFirst { it == chip.text }
            val next = currencyArray[first]
            if (currency != next) {
                currency_icon.setImageDrawable(getNextAnimatedIcon(currency, next))
                val drawable = currency_icon.drawable
                if (drawable is Animatable) drawable.start()
                currency = next
            }
        }

        currency_icon.setOnClickListener {
            if (isClickable) {
                val position = (currencyArray.indexOfFirst { it == currency } + 1) % currencyArray.size
                val next = currencyArray[position]
                chipsComponent.setSelected(next)
                currency_icon.setImageDrawable(getNextAnimatedIcon(currency, next))
                val drawable = currency_icon.drawable
                if (drawable is Animatable) drawable.start()

                currency = next
                if (android.os.Build.VERSION.SDK_INT >= 21) {
                    isClickable = false
                    currency_icon?.postDelayed({ isClickable = true }, 605)
                }
            }
        }

        done.setOnClickListener { submit() }
        name.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                submit()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }

    }

    override fun updateUI() {
        if (source.id > 0) {
            name.setText(source.name)
            currency = source.currency
            done.setText(R.string.done)
        }

        chipsComponent.setSelected(currency)

        currency_icon.setImageDrawable(getCurrencyImage(currency))
    }

    private fun getCurrencyImage(current: String): Drawable {
        val id = when (current) {
            "USD" -> R.drawable.currency_usd
            "EUR" -> R.drawable.currency_eur
            "UAH" -> R.drawable.currency_uah
            else -> 0
        }
        return resources.getDrawable(id)
    }

    fun getNextAnimatedIcon(current: String, next: String): Drawable {
        val id = when (current) {
            "USD" -> if (next == "UAH") R.drawable.currency_usd_to_uah else R.drawable.currency_usd_to_eur
            "EUR" -> if (next == "UAH") R.drawable.currency_eur_to_uah else R.drawable.currency_eur_to_usd
            "UAH" -> if (next == "USD") R.drawable.currency_uah_to_usd else R.drawable.currency_uah_to_eur
            else -> 0
        }
        return resources.getDrawable(id)
    }

    private fun submit() {

        if (verifyName(name)) {

            source.name = name.text.toString().trim()
            source.currency = currency

            AppService.save(source)
            EventBus.update(SourceListFragment::class.java)
            hideKeyboard()

            close()
        }

    }

    private fun verifyName(name: TextView): Boolean {
        if (name.text.toString().trim().isEmpty()) {
            name.error = resources.getString(R.string.error_should_be_not_empty)
            return false
        }

        if (name.text.toString().trim().length in 3..40) {
        } else {
            name.error = resources.getString(R.string.error_length_from_3_to_40)
            return false
        }

        if (!regexpName.matcher(name.text.toString()).matches()) {
            name.error = resources.getString(R.string.error_not_correct_symbols)
            return false
        }

        return true
    }

}