package basilliy.cashcard.module.source

import android.os.Bundle
import basilliy.cashcard.R
import basilliy.cashcard.Source
import basilliy.cashcard.Transaction
import basilliy.cashcard.module.base.BaseActivity
import basilliy.cashcard.module.base.BaseSecondFragment
import basilliy.cashcard.module.base.EventBus
import basilliy.cashcard.module.base.ViewController
import basilliy.cashcard.service.AppService
import basilliy.cashcard.todayMillis
import basilliyc.extensions.afterChange
import basilliyc.extensions.onAction
import basilliyc.extensions.textValue
import basilliyc.extensions.tryOrNull
import kotlinx.android.synthetic.main.transaction_type_dialog.*

class TransactionFragment : BaseSecondFragment() {

    companion object {

        private const val KEY_TRANSACTION_ID = "KEY_TRANSACTION_ID"
        private const val KEY_SOURCE_ID = "KEY_SOURCE_ID"

        fun newInstance(sourceId: Long, transactionId: Long): TransactionFragment {
            val fragment = TransactionFragment()
            val bundle = Bundle()

            bundle.putLong(KEY_SOURCE_ID, sourceId)
            bundle.putLong(KEY_TRANSACTION_ID, transactionId)

            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var transaction: Transaction
    private lateinit var source: Source

    override fun onBackPressedExecute(): Boolean {
        animate(false) {
            val fragmentActivity = activity
            if (fragmentActivity is SourceHistoryActivity) {
                fragmentActivity.removeSecondFragment()
            }
        }
        return false
    }

    override fun close(animate: Boolean) {
        if (animate) {
            animate(false) {
                val activity = activity
                if (activity is SourceHistoryActivity)
                    activity.removeSecondFragment()
            }
        } else {
            val activity = activity
            if (activity is SourceHistoryActivity)
                activity.removeSecondFragment()
        }
    }

    override fun setSecondContentView(): Int = R.layout.transaction_type_dialog

    override fun getUpdateStrategy(): ViewController.UpdateStrategy {
        return ViewController.UpdateStrategy.Always
    }

    override fun initValues() {
        super.initValues()

        source = AppService.getSource(arguments!!.getLong(KEY_SOURCE_ID))!!
        transaction = AppService.getTransaction(arguments!!.getLong(KEY_TRANSACTION_ID))!!
    }

    override fun initUI() {
        super.initUI()

        transaction_value.setText(transaction.value.toString())
        transaction_value.requestFocus()
        (activity as? BaseActivity)?.showKeyboard(transaction_value)
    }

    override fun initListeners() {
        super.initListeners()

        transaction_value.afterChange {
            updateUI()
        }

        transaction_value.onAction {
            submit()
        }

        done.setOnClickListener {
            submit()
        }

        delete.setOnClickListener {
            delete()
        }

    }

    override fun updateUI() {

    }

    private fun submit() {
        val deviation = tryOrNull { transaction_value.textValue(true).toInt() } ?: 0

        if (deviation != 0) {
            source.balance -= transaction.value - deviation
            transaction.value = deviation

            AppService.save(transaction)

            AppService.getAsyncTransactionSumForTodayBySourceId(source.id) {
                source.forTodayValue = it
                source.forTodayTime = todayMillis()
            }

            AppService.save(source)

            EventBus.update(SourceHistoryActivity::class.java)
            ViewController.update(SourceListFragment::class.java)
            close()
            hideKeyboard()

        } else {
            delete()
        }

    }

    private fun delete() {
        AppService.delete(transaction)

        source.balance -= transaction.value

        AppService.getAsyncTransactionSumForTodayBySourceId(source.id) {
            source.forTodayValue = it
            source.forTodayTime = todayMillis()
        }

        AppService.save(source)

        ViewController.update(SourceListFragment::class.java)
        EventBus.update(SourceHistoryActivity::class.java)
        close()
        hideKeyboard()
    }

}