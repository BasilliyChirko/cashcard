package basilliy.cashcard.module.source

import android.annotation.SuppressLint
import basilliy.cashcard.R
import basilliy.cashcard.format
import basilliy.cashcard.module.base.BaseFragment
import basilliy.cashcard.module.base.ViewController
import basilliy.cashcard.module.navigation.NavigateActivity
import basilliy.cashcard.service.AppService
import basilliy.cashcard.service.CurrencyService
import basilliyc.extensions.init
import kotlinx.android.synthetic.main.source_list_fragment.*

@SuppressLint("RestrictedApi")
class SourceListFragment : BaseFragment() {

    lateinit var adapter: SourceRecyclerAdapter

    override fun setContentView(): Int = R.layout.source_list_fragment

    override fun getUpdateStrategy(): ViewController.UpdateStrategy {
        return ViewController.UpdateStrategy.Always
    }

    override fun initValues() {
        super.initValues()
        adapter = SourceRecyclerAdapter()
    }

    override fun initUI() {
        super.initUI()
        recycler.init(adapter = adapter, itemAnimator = null)

        CurrencyService.getUSD {
            currency_usd.text = it.format(2)
        }

        CurrencyService.getEUR {
            currency_eur.text = it.format(2)
        }
    }

    override fun initListeners() {
        super.initListeners()

        adapter.onClick {
            if (it == null) onClickCreateNew()
            else {
                val deviationFragment = DeviationFragment.newInstance(it.id)
                deviationFragment.show(childFragmentManager, "deviation")
            }
        }

        adapter.onLongClick {
            if (it == null) onClickCreateNew()
            else (activity as? NavigateActivity)?.setSecondPage(SourceOptionsFragment.newInstance(it.id))
        }

    }

    private fun onClickCreateNew() {
        (activity as? NavigateActivity)?.setSecondPage(SourceFormFragment.newInstance())
    }

    override fun updateUI() {
        adapter.setData(AppService.getSourceAll())
    }

}