package basilliy.cashcard.module.source

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import androidx.fragment.app.DialogFragment
import basilliy.cashcard.R
import basilliy.cashcard.Source
import basilliy.cashcard.module.base.ViewController
import basilliy.cashcard.service.AppService
import basilliy.cashcard.service.save
import basilliyc.extensions.inflate
import basilliyc.extensions.move
import kotlinx.android.synthetic.main.source_position_dialog.view.*

class SourcePositionDialog : DialogFragment() {

    lateinit var source: Source

    private lateinit var root: View

    private lateinit var allSources: ArrayList<Source>

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = activity?.inflate(R.layout.source_position_dialog)!!
        root = view

        allSources = AppService.getSourceAll()

        view.position_seek.max = allSources.lastIndex
        view.position_seek.progress = source.positionOnList
        setLabel(source.positionOnList)

        view.position_seek.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                setLabel(progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onStopTrackingTouch(seekBar: SeekBar?) {}

        })

        view.sumbit.setOnClickListener {
            applyNewPosition(view.position_seek.progress)
        }

        return AlertDialog.Builder(activity).setView(view).create()
    }

    private fun setLabel(value: Int) {
        root.position_value.text = (value + 1).toString()
    }

    private fun applyNewPosition(newPosition: Int) {
        val find = allSources.indexOfFirst { it.id == source.id }
        allSources.move(find, newPosition)

        allSources.forEachIndexed { index, source ->
            source.positionOnList = index
            source.save()
        }

        dismiss()
        ViewController.update(SourceListFragment::class.java, immediately = true)
    }

}