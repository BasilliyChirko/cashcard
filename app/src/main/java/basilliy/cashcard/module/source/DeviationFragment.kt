package basilliy.cashcard.module.source

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import basilliy.cashcard.R
import basilliy.cashcard.Source
import basilliy.cashcard.module.base.EventBus
import basilliy.cashcard.service.AppService
import basilliy.cashcard.withSpaces
import basilliyc.extensions.*
import kotlinx.android.synthetic.main.source_deviation_fragment.view.*


class DeviationFragment : DialogFragment() {

    companion object {

        private const val KEY_SOURCE_ID = "KEY_SOURCE_ID"

        fun newInstance(sourceId: Long): DeviationFragment {
            val fragment = DeviationFragment()
            val bundle = Bundle()

            bundle.putLong(KEY_SOURCE_ID, sourceId)

            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var source: Source
    private lateinit var rootView: View
    lateinit var consumptionAdapter: ConsumptionRecyclerAdapter

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        // Init values
        val sourceId = arguments!!.getLong(KEY_SOURCE_ID)
        source = AppService.getSource(sourceId)!!

        val forTodayTransactions = AppService.getTransactionBySourceIdForToday(sourceId)

        // Inflate root view
        rootView = activity!!.inflate(R.layout.source_deviation_fragment)

        if (forTodayTransactions.isNotEmpty()) {
            consumptionAdapter = ConsumptionRecyclerAdapter(activity!!, source.id, source.currencySymbol) {}
            consumptionAdapter.dateFormat = "HH:mm"
            consumptionAdapter.data = forTodayTransactions
            consumptionAdapter.data.sortByDescending { it.time }

            rootView.for_today_deviation_list.adapter = consumptionAdapter
            val linearLayoutManager = LinearLayoutManager(activity!!)
            rootView.for_today_deviation_list.layoutManager = linearLayoutManager
            rootView.for_today_deviation_list.itemAnimator = DefaultItemAnimator()
        } else {
            rootView.deviation_for_today_layout.visible = false
        }

        rootView.deviation_value.afterChange {
            updateUI()
        }

        rootView.deviation_value.onAction {
            submit()
        }

        rootView.deviation_submit.setOnClickListener {
            submit()
        }

        rootView.deviation_source_name.text = source.name
        rootView.deviation_value.requestFocus()


        return AlertDialog.Builder(activity!!).setView(rootView).create()
    }

    override fun onResume() {
        super.onResume()
        rootView.post {
            rootView.deviation_value.requestFocus()
            val imgr = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imgr.showSoftInput(rootView.deviation_value, InputMethodManager.SHOW_IMPLICIT)
        }
        updateUI()
    }


    fun updateUI() {
        updateDeviationTypeLabel()
        updateSourceResultBalance()
    }

    @SuppressLint("SetTextI18n")
    private fun updateSourceResultBalance() {
        val deviation = tryOrNull { rootView.deviation_value.textValue(true).toInt() } ?: 0
        val resultBalance = source.balance + deviation

        rootView.deviation_source_balance_value.text = source.currencySymbol + " " + resultBalance.withSpaces()
        rootView.deviation_source_balance_value.setTextColor(resources.getColor(
                if (resultBalance >= 0) R.color.black
                else R.color.accent
        ))

    }

    private fun updateDeviationTypeLabel() {
        val textValue = rootView.deviation_value.textValue(true)
        when {
            textValue.startsWith("+") -> rootView.deviation_type.setText(R.string.deviation_plus)
            textValue.startsWith("-") -> rootView.deviation_type.setText(R.string.deviation_minus)
            else -> rootView.deviation_type.setText(R.string.deviation_plus)
        }
    }

    private fun submit() {
        val deviation = tryOrNull { rootView.deviation_value.textValue(true).toInt() } ?: 0

        if (deviation == 0) {
            dismiss()
            return
        }

        AppService.addDeviation(source, deviation) {
            dismiss()
            EventBus.update(SourceListFragment::class.java)
        }

    }

}