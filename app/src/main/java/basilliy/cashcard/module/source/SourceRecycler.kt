package basilliy.cashcard.module.source

import android.annotation.SuppressLint
import android.view.View
import basilliy.cashcard.R
import basilliy.cashcard.Source
import basilliy.cashcard.toStringWithSign
import basilliy.cashcard.withSpaces
import basilliyc.extensions.BaseRecyclerAdapter
import basilliyc.extensions.BaseRecyclerHolder
import basilliyc.extensions.toArrayList
import kotlinx.android.synthetic.main.source_element.view.*

class SourceRecyclerAdapter : BaseRecyclerAdapter<Source?>() {

    private val type_normal = 0
    private val type_mock = 1

    override fun getItemViewType(position: Int): Int {
        return if (data.lastIndex == position) type_mock else type_normal
    }

    override fun getLayout(type: Int): Int = if (type == type_normal) R.layout.source_element else R.layout.source_element_mock

    override fun setData(list: List<Source?>, animate: Boolean, clear: Boolean): BaseRecyclerAdapter<Source?> {
        val arrayList = list.toArrayList()
        arrayList.add(null)
        return super.setData(arrayList, animate, clear)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindHolder(holder: BaseRecyclerHolder<Source?>, item: Source?) {
        if (item == null) {
            return
        }

        with(holder.view) {
            name.text = item.name
            balance.text = item.currencySymbol + " " + item.balance.withSpaces()

            balance.setTextColor(resources.getColor(
                    if (item.balance >= 0) R.color.black
                    else R.color.accent
            ))

            val deviation = item.getTodayDeviation()

            if (deviation != 0) {

                for_today_label.visibility = View.VISIBLE
                for_today_value.visibility = View.VISIBLE

                for_today_value.text = deviation.toStringWithSign() + " " + item.currencySymbol

                for_today_value.setTextColor(resources.getColor(
                        if (deviation > 0) R.color.primaryDark
                        else R.color.accent
                ))

            } else {
                for_today_label.visibility = View.INVISIBLE
                for_today_value.visibility = View.INVISIBLE
            }

        }
    }

}

