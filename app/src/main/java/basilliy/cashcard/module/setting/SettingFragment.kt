package basilliy.cashcard.module.setting

import android.annotation.SuppressLint
import basilliy.cashcard.R
import basilliy.cashcard.module.base.BaseActivity
import basilliy.cashcard.module.base.BaseFragment
import basilliy.cashcard.module.base.ViewController
import basilliy.cashcard.module.components.createRequestDialog
import basilliy.cashcard.module.components.hideRequestDialog
import basilliy.cashcard.service.AppService
import basilliyc.extensions.formatTime
import basilliyc.extensions.toast
import com.codekidlabs.storagechooser.StorageChooser
import kotlinx.android.synthetic.main.fragment_setting.*
import org.json.JSONObject
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

@SuppressLint("RestrictedApi", "SetTextI18n")
class SettingFragment : BaseFragment() {

    override fun setContentView(): Int = R.layout.fragment_setting

    override fun getUpdateStrategy(): ViewController.UpdateStrategy {
        return ViewController.UpdateStrategy.Always
    }

    override fun initListeners() {
        super.initListeners()

        icon_export.setOnClickListener { exportData() }
        text_export.setOnClickListener { exportData() }
        icon_import.setOnClickListener { importData() }
        text_import.setOnClickListener { importData() }
    }

    private fun exportData() {
        (activity as? BaseActivity)?.withDiskPermission {
            val chooser = StorageChooser.Builder()
                    .withActivity(activity)
                    .withFragmentManager(activity!!.fragmentManager)
                    .allowCustomPath(true)
                    .setType(StorageChooser.DIRECTORY_CHOOSER)
                    .setInternalStorageText(resources.getString(R.string.dialog_internal_storage))
                    .actionSave(true)
                    .setDialogTitle(resources.getString(R.string.dialog_choose_storage))
                    .build()
            chooser.show()

            chooser.setOnSelectListener { dirPath ->
                try {
                    val fileName = "export-" + System.currentTimeMillis().formatTime("yyyy-MM-dd-HH-mm") + ".json"
                    val file = File(dirPath, fileName)
                    val string = AppService.createDataJSON().toString().toByteArray()
                    FileOutputStream(file).use { it.write(string) }
                    activity?.toast("Сохранено $fileName")
                } catch (e: Exception) {
                    e.printStackTrace()
                    activity?.toast("Ошибка")
                }
            }

        }

    }

    private fun importData() {
        (activity as? BaseActivity)?.withDiskPermission {
            val chooser = StorageChooser.Builder()
                    .withActivity(activity)
                    .withFragmentManager(activity!!.fragmentManager)
                    .allowCustomPath(true)
                    .setType(StorageChooser.FILE_PICKER)
                    .setInternalStorageText(resources.getString(R.string.dialog_internal_storage))
                    .setDialogTitle(resources.getString(R.string.dialog_choose_storage))
                    .build()

            chooser.show()

            chooser.setOnSelectListener { path ->
                try {
                    activity?.createRequestDialog("Импорт")
                    val file = File(path)

                    val length = file.length().toInt()
                    val bytes = ByteArray(length)
                    FileInputStream(file).use { it.read(bytes) }
                    AppService.readDataJSON(JSONObject(String(bytes))) {
                        activity?.hideRequestDialog()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    context?.toast("Ошибка")
                }
            }
        }
    }

}