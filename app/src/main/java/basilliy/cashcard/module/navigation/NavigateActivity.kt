package basilliy.cashcard.module.navigation

import android.annotation.SuppressLint
import basilliy.cashcard.Preferences
import basilliy.cashcard.R
import basilliy.cashcard.module.base.BaseActivity
import basilliy.cashcard.module.base.BaseFragment
import basilliy.cashcard.module.setting.SettingFragment
import basilliy.cashcard.module.source.SourceListFragment
import basilliyc.extensions.postDelayed
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.app_bar_navigate.*

@Suppress("BooleanLiteralArgument")
@SuppressLint("RestrictedApi")
class NavigateActivity : BaseActivity() {

    private var currentFragment: BaseFragment? = null
    private var currentFragmentSecond: BaseFragment? = null

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.nav_source -> {
                setPage(PageType.SOURCE)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_setting -> {
                setPage(PageType.SETTING)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun setContentView(): Int = R.layout.app_bar_navigate

    override fun initUI() {
        super.initUI()

        supportActionBar?.hide()
        mainRoot.setBackgroundColor(resources.getColor(R.color.greyUltraLight))

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        postDelayed(0) {
            navigation.selectedItemId = R.id.nav_source
        }

        title = ""

    }

    private fun setPage(type: PageType) {

        if (Preferences.navigatePage == type && currentFragment != null) {
            return
        }

        val fragment = when (type) {
            PageType.SETTING -> SettingFragment()
            PageType.SOURCE -> SourceListFragment()
        }

        Preferences.navigatePage = type

        setPage(fragment, true, false)

    }

    fun setPage(fragment: BaseFragment, animate: Boolean = true, isSecond: Boolean = false) {
        val transaction = supportFragmentManager.beginTransaction()

        if (!isSecond) {
            if (animate) transaction.setCustomAnimations(
                    android.R.animator.fade_in,
                    android.R.animator.fade_out,
                    android.R.animator.fade_in,
                    android.R.animator.fade_out
            ) else transaction.setCustomAnimations(
                    0,
                    0,
                    android.R.animator.fade_in,
                    android.R.animator.fade_out
            )

            currentFragment = fragment
            transaction.replace(R.id.container, fragment)

        } else {
            currentFragmentSecond = fragment
            transaction.replace(R.id.navigation_content_second, fragment)
        }

        transaction.commit()
    }

    fun setSecondPage(fragment: BaseFragment) {
        setPage(fragment, animate = false, isSecond = true)
    }

    override fun onBackPressed() {
        if (currentFragmentSecond != null) {
            currentFragmentSecond!!.onBackPressedExecute()
            return
        }

        currentFragment?.let {
            if (!it.onBackPressedExecute()) {
                if (Preferences.navigatePage == PageType.SOURCE) super.onBackPressed()
                else navigation.selectedItemId = R.id.nav_source
            }
        }
    }

    fun removeSecondFragment() {
        if (currentFragmentSecond != null) {
            supportFragmentManager.beginTransaction().remove(currentFragmentSecond!!).commit()
            currentFragmentSecond = null
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        currentFragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

}
