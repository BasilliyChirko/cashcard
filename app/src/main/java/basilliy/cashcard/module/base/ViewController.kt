package basilliy.cashcard.module.base

interface ViewController {

    /**
     * Return a resource id of layout witch should be inflated
     */
    fun setContentView(): Int

    /**
     * Initial values for lateinit variables
     *
     * Invoke once (onCreate)
     */
    fun initValues()

    /**
     * Initial default params of views,
     * such as adapter and layout manager on recycler view
     *
     * Invoke once (onViewCreated)
     */
    fun initUI()

    /**
     * Update data in UI views
     *
     * Invoke if isViewShouldBeUpdated (onViewCreated), after initUI
     */
    fun updateUI()

    /**
     * Initial listeners for use.
     * Prefer for small callback (one-two lines), if callback have large implementation
     * should invoke own function with this implementation
     *
     * Invoke once (onViewCreated), after updateUI
     */
    fun initListeners()

    /**
     * Set a type of algorithm to update data
     */
    fun getUpdateStrategy(): UpdateStrategy

    /**
     * A time in millis after last update data when data should be updated
     */
    fun getTimeoutForUpdateData(): Long = 0L

    enum class UpdateStrategy {
        Always, Never, Once, OnTimeout
    }

    companion object {

        private var currentPages = HashSet<Class<*>>()

        fun setPageAsCurrent(clazz: Class<*>) {
            currentPages.add(clazz)
        }

        fun removePageFromCurrent(clazz: Class<*>) {
            currentPages.remove(clazz)
        }

        fun isCurrentPage(clazz: List<Class<*>>): Boolean {
            clazz.forEach {
                if (currentPages.contains(it)) {
                    return true
                }
            }
            return false
        }

        private val controllerList = HashSet<Class<*>>()

        fun update(clazz: Class<*>, immediately: Boolean = false) {
            if (immediately) {
                EventBus.sendMessage(clazz, action = EventBus.UPDATE)
            } else {
                controllerList.add(clazz)
            }
        }

        fun isNeedUpdate(clazz: Class<*>) = controllerList.remove(clazz)

    }

}