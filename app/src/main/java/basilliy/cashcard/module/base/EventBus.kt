@file:Suppress("DEPRECATION")

package basilliy.cashcard.module.base

object EventBus {

    data class Message(
            var action: String?,
            var data: Any? = null
    ) {
        fun haveKey(action: String?) = this.action == action
    }

    interface Listener {
        fun receiveMassage(message: Message)
    }

    private val listenerList = HashMap<Class<*>, (message: Message) -> Unit>()

    fun addListener(receiver: Class<*>, call: (message: Message) -> Unit) {
        listenerList[receiver] = call
    }

    fun addListener(receiver: Any, call: (message: Message) -> Unit) {
        listenerList[receiver::class.java] = call
    }

    fun removeListener(receiver: Class<*>) {
        listenerList.remove(receiver)
    }

    fun removeListener(receiver: Any) {
        listenerList.remove(receiver::class.java)
    }

    fun sendMessage(receiver: Class<*>, action: String? = null, data: Any? = null) {
        listenerList[receiver]?.invoke(Message(action = action, data = data))
    }

    fun update(receiver: Class<*>) {
        sendMessage(receiver, UPDATE)
    }

    const val UPDATE = "UPDATE"

}



