package basilliy.cashcard.module.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import basilliy.cashcard.Constants

abstract class BaseFragment : Fragment(), ViewController {

    private var lastUseTime = 0L
    var isViewShouldBeUpdated = true
        private set

    private var isViewShouldBeInitialized = true
    private var isListenersShouldBeInitialized = true

    var saveView: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initValues()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val strategy = getUpdateStrategy()
        when (strategy) {
            ViewController.UpdateStrategy.Always -> isViewShouldBeUpdated = true
            ViewController.UpdateStrategy.Never -> isViewShouldBeUpdated = false
            ViewController.UpdateStrategy.Once -> isViewShouldBeUpdated = saveView == null
            ViewController.UpdateStrategy.OnTimeout -> {
                val currentTimeMillis = System.currentTimeMillis()
                if (currentTimeMillis - lastUseTime > getTimeoutForUpdateData()) {
                    isViewShouldBeUpdated = true
                    lastUseTime = currentTimeMillis
                } else {
                    isViewShouldBeUpdated = false
                }
            }
        }

        if (!isViewShouldBeUpdated) {
            isViewShouldBeUpdated = ViewController.isNeedUpdate(this::class.java)
        }

        if (reuseView() && saveView != null) {
            return saveView
        } else {
            isViewShouldBeInitialized = true
            isListenersShouldBeInitialized = true
            isViewShouldBeUpdated = true
        }

        val contentView = setContentView()
        if (contentView != 0) {
            return inflater.inflate(contentView, container, false)
        }

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        saveView = view

        if (isViewShouldBeInitialized) {
            initUI()
            isViewShouldBeInitialized = false
        }

        if (isViewShouldBeUpdated) {
            ViewController.isNeedUpdate(this::class.java)
            updateUI()
        }

        if (isListenersShouldBeInitialized) {
            initListeners()
            isListenersShouldBeInitialized = false
        }
    }

    override fun onResume() {
        super.onResume()

        if (ViewController.isNeedUpdate(this::class.java)) {
            updateUI()
        }

        ViewController.setPageAsCurrent(this::class.java)
        EventBus.addListener(this::class.java) {
            when (it.action) {
                EventBus.UPDATE -> view?.post {
                    updateUI()
                }
                else -> onReceiveMassage(it)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        ViewController.removePageFromCurrent(this::class.java)
        EventBus.removeListener(this::class.java)
    }

    open fun onReceiveMassage(message: EventBus.Message) {}

    override fun setContentView(): Int = 0

    override fun initValues() {}

    override fun initUI() {}

    override fun updateUI() {}

    override fun initListeners() {}

    override fun getUpdateStrategy(): ViewController.UpdateStrategy = ViewController.UpdateStrategy.Once

    override fun getTimeoutForUpdateData(): Long = Constants.Fragment.fragmentUseTime

    open fun onBackPressed() = activity?.onBackPressed()

    open fun onBackPressedExecute(): Boolean = false

    open fun reuseView(): Boolean = true

    fun hideKeyboard() {
        val activity = activity
        if (activity is BaseActivity) {
            activity.hideKeyboard()
        }
    }
}