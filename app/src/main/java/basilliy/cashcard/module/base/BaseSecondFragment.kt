package basilliy.cashcard.module.base

import android.animation.ValueAnimator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import basilliy.cashcard.R
import basilliy.cashcard.module.navigation.NavigateActivity
import kotlinx.android.synthetic.main.base_page_second.*

abstract class BaseSecondFragment : BaseFragment() {

    companion object {
        const val animationDuration = 250L
    }

    override fun setContentView(): Int = R.layout.base_page_second

    override fun initUI() {
        super.initUI()
        applyWithSecondView()
        animate(true)
    }

    override fun initListeners() {
        super.initListeners()
        second_layout.setOnClickListener { onBackPressed() }
    }

    override fun onBackPressedExecute(): Boolean {
        animate(false) {
            val fragmentActivity = activity
            if (fragmentActivity is NavigateActivity) {
                fragmentActivity.removeSecondFragment()
            }
        }
        return false
    }

    protected fun animate(show: Boolean, callback: (() -> Unit)? = null) {
        val start = if (show) 0F else 1F
        val end = if (show) 1F else 0F
        var height = 0F
        val animator = ValueAnimator.ofFloat(start, end)
        animator.duration = animationDuration

        val background = second_background
        val content = second_content

        animator.addUpdateListener {
            val value = it.animatedValue as Float
            background.alpha = value

            if (height == 0F) {
                if (content.height == 0) {
                    content.translationY = 5000F
                } else {
                    height = content.height.toFloat()
                }
            } else {
                content.translationY = height * (1F - value)
            }

            if (value == end && callback != null) {
                callback()
            }
        }

        if (content != null && background != null)
            animator.start()
    }

    abstract fun setSecondContentView(): Int

    open fun close(animate: Boolean = true) {
        if (animate) {
            animate(false) {
                val activity = activity
                if (activity is NavigateActivity)
                    activity.removeSecondFragment()
            }
        } else {
            val activity = activity
            if (activity is NavigateActivity)
                activity.removeSecondFragment()
        }
    }

    private fun applyWithSecondView() {
        second_content.removeAllViews()
        second_content.addView(inflate(setSecondContentView()))
    }

    fun BaseFragment.inflate(layoutId: Int, root: ViewGroup? = null, attachToRoot: Boolean = false): View {
        return LayoutInflater.from(this.context).inflate(layoutId, root, attachToRoot)
    }

}