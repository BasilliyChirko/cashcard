package basilliy.cashcard.module.base

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import basilliy.cashcard.Constants
import basilliy.cashcard.R
import basilliy.cashcard.database.Database


abstract class BaseActivity : AppCompatActivity(), ViewController {

    private var lastUseTime = 0L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (setContentView() != 0) {
            setContentView(setContentView())
        }

        initValues()
        initUI()
        initListeners()

        if (getUpdateStrategy() == ViewController.UpdateStrategy.Once) {
            updateUI()
        }
    }

    @Suppress("NON_EXHAUSTIVE_WHEN")
    override fun onResume() {
        super.onResume()

        ViewController.setPageAsCurrent(this::class.java)
        EventBus.addListener(this::class.java) {
            when (it.action) {
                EventBus.UPDATE -> updateUI()
                else -> onReceiveMessage(it)
            }
        }

        if (ViewController.isNeedUpdate(this::class.java)) {
            updateUI()
            return
        }

        val strategy = getUpdateStrategy()
        when (strategy) {
            ViewController.UpdateStrategy.Always -> updateUI()
            ViewController.UpdateStrategy.OnTimeout -> {
                val currentTimeMillis = System.currentTimeMillis()
                if (currentTimeMillis - lastUseTime > getTimeoutForUpdateData()) {
                    lastUseTime = currentTimeMillis
                    updateUI()
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        ViewController.removePageFromCurrent(this::class.java)
        EventBus.removeListener(this::class.java)
        Database.close()
    }

    open fun onReceiveMessage(message: EventBus.Message) {

    }

    override fun setContentView(): Int = 0

    override fun initValues() {}

    override fun initUI() {}

    override fun updateUI() {}

    override fun initListeners() {}

    override fun getUpdateStrategy(): ViewController.UpdateStrategy = ViewController.UpdateStrategy.Once

    override fun getTimeoutForUpdateData(): Long = Constants.Fragment.fragmentUseTime

    fun hideKeyboard() {
        val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        val view = this.currentFocus ?: View(this)
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showKeyboard(focus: View? = null, flag: Int = InputMethodManager.SHOW_IMPLICIT) {
        val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        val view = focus ?: this.currentFocus ?: View(this)
        imm.showSoftInput(view, flag)
    }

    fun Activity.inflate(layoutId: Int, root: ViewGroup? = null, attachToRoot: Boolean = false): View {
        return LayoutInflater.from(this).inflate(layoutId, root, attachToRoot)
    }


    //Check disk permission

    private var permissionResult: ((granted: Boolean) -> Unit)? = null

    private val checkExternalDiskPermissionRequest = 8361

    fun withDiskPermission(onGranted: () -> Unit) {
        checkExternalDiskPermission {
            if (it) {
                onGranted()
            } else {
                val builder = AlertDialog.Builder(this)
                builder.setMessage(R.string.need_disk_permission)
                builder.setPositiveButton("OK") { dialog, _ ->
                    dialog.dismiss()
                }
                builder.create().show()
            }
        }
    }

    private fun checkExternalDiskPermission(result: (granted: Boolean) -> Unit) {
        permissionResult = result
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                result(false)
            } else {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        checkExternalDiskPermissionRequest)
            }
        } else {
            result(true)
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            checkExternalDiskPermissionRequest -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    permissionResult?.invoke(true)
                } else {
                    permissionResult?.invoke(false)
                }
                return
            }
            else -> {
            }
        }
    }
}