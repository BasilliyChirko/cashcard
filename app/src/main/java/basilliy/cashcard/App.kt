package basilliy.cashcard

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex


class App : Application() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        app = this
        Preferences.setup = true

        if (Preferences.firstLogin) {
            Preferences.clean()
            Preferences.firstLogin = false
        }
    }

    companion object {

        lateinit var app: App

        fun getContext(): Context {
            return app.applicationContext
        }

    }

}