@file:Suppress("DEPRECATION", "unused")

package basilliyc.extensions

import android.animation.Animator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/** LOGGER Proger*/

const val NOT_IMPLEMENTED = "Not implemented"

const val TAG = "AndroidRuntime"

fun Context.toast(x: Any?) {

    val message = when (x) {
        null -> "null"
        is Int -> resources.getString(x)
        is String -> x
        else -> x.toString()
    }

    val toast = Toast.makeText(
            this,
            message,
            if (message.length > 32) Toast.LENGTH_LONG else Toast.LENGTH_SHORT
    )

    val view = toast.view
    if (view is LinearLayout) {
        if (view.childCount >= 1) {
            val text = view.getChildAt(0)
            if (text is TextView) {
                text.gravity = Gravity.CENTER
            }
        }
    }

    toast.show()
}

fun Fragment.toast(x: Any?) {
    activity?.toast(x)
}

inline fun logBuilder(builderAction: StringBuilder.() -> Unit) =
        log(StringBuilder().apply(builderAction).toString())

fun log(x: Any? = null) {
    when (x) {
        null -> Log.i(TAG, "null")
        else -> Log.i(TAG, x.toString())
    }
}

fun Any.log(x: Any? = this) {
    when (x) {
        null -> Log.i(TAG, "null")
        else -> Log.i(TAG, x.toString())
    }
}

fun Any.error(x: Any? = this) {
    when (x) {
        null -> Log.e(TAG, "null")
        else -> Log.e(TAG, x.toString())
    }
}

fun Any.debug(x: Any? = this) {
    when (x) {
        null -> Log.d(TAG, "null")
        else -> Log.d(TAG, x.toString())
    }
}

fun <T> Collection<T>.logAll() {
    this.forEach {
        log(it)
    }
}

fun <T> List<T>.logAll(call: (T) -> String?) {
    this.forEach {
        basilliyc.extensions.log(call(it))
    }
}

fun <K, V> Map<K, V>.logAll(call: (Map.Entry<K, V>) -> String?) {
    this.forEach {
        basilliyc.extensions.log(call(it))
    }
}


/** ACTIVITY */

fun <T> Activity.goToActivity(clazz: Class<T>, finish: Boolean = false) {
    start(clazz)
    if (finish)
        finish()
}

fun <T> Activity.goToActivity(clazz: Class<T>, request: Int) {
    startActivityForResult(Intent(this, clazz), request)
}

fun <T> Fragment.goToActivity(clazz: Class<T>) {
    activity?.start(clazz)
}

fun <T> Context.start(clazz: Class<T>) {
    startActivity(Intent(this, clazz))
}


/** TIME */

const val formatYearMonthDayHourMinuteSeconds = "yyyy-MM-dd HH:mm:ss"
const val formatYearMonthDayHourMinute = "yyyy-MM-dd HH:mm"
const val formatYearMonthDay = "yyyy-MM-dd"

fun Long.toCalendar(): Calendar {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    return calendar
}

const val secInMillis = 1000L
const val minInMillis = 1000L * 60L
const val hourInMillis = 1000L * 60L * 60L
const val dayInMillis = 1000L * 60L * 60L * 24L
const val monthInMillis = 1000L * 60L * 60L * 24L * 30L
const val yearInMillis = 1000L * 60L * 60L * 24L * 30L * 12L


/** STRING */

@SuppressLint("SimpleDateFormat")
fun Long.printTime(): String {
    return SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Date(this))
}

@SuppressLint("SimpleDateFormat")
fun Long.formatTime(pattern: String): String {
    return tryOrNull { SimpleDateFormat(pattern).format(Date(this)) } ?: ""
}

@SuppressLint("SimpleDateFormat")
fun String.formatTime(pattern: String): Long {
    return tryOrNull { SimpleDateFormat(pattern).parse(this).time } ?: 0L
}


/** SERVICE */

fun createBroadcastReceiver(call: (intent: Intent) -> Unit): BroadcastReceiver {
    return object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let(call)
        }
    }
}


/** ANIMATION */

fun View.animateVisibility(show: Boolean, duration: Long = 300, hideType: Int = View.GONE) {
    visibility = if (show) View.VISIBLE else hideType
    alpha = if (show) 0f else 1f
    animate()
            .alpha(if (show) 1f else 0f)
            .setDuration(duration)
            .setInterpolator(AccelerateDecelerateInterpolator())
            .start()
}

fun View.visibility(show: Boolean, hideType: Int = View.GONE) {
    visibility = if (show) View.VISIBLE else hideType
}

var View.visible: Boolean
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }
    get() = visibility == View.VISIBLE

/** EDIT TEXT */
fun EditText.validate(error: String, haveError: (String) -> Boolean): Boolean {
    val isError = haveError(this.text.toString().trim())
    if (isError) this.error = error
    return !isError
}

fun EditText.validate(error: Int, showErrorMessage: Boolean = true, haveError: (String) -> Boolean): Boolean {
    val isError = haveError(this.text.toString().trim())
    if (isError && showErrorMessage) this.error = context.resources.getString(error)
    return !isError
}

fun EditText.validateNoTrim(error: Int, haveError: (String) -> Boolean): Boolean {
    val isError = haveError(this.text.toString())
    if (isError) this.error = context.resources.getString(error)
    return !isError
}

fun EditText.textValue(trim: Boolean = false): String {
    return if (trim) text.toString().trim() else text.toString()
}

//fun EditText.validate(haveError: (String) -> Boolean): Boolean {
//    return !haveError(this.text.toString().trim())
//}

inline fun EditText.onAction(crossinline callback: () -> Unit) {
    setOnEditorActionListener { _, i, _ ->
        if (i == EditorInfo.IME_ACTION_DONE ||
                i == EditorInfo.IME_ACTION_NEXT ||
                i == EditorInfo.IME_ACTION_GO ||
                i == EditorInfo.IME_ACTION_SEARCH ||
                i == EditorInfo.IME_ACTION_SEND) {
            callback()
            return@setOnEditorActionListener true
        }
        false
    }
}

inline fun View.onClick(crossinline callback: (view: View) -> Unit) {
    setOnClickListener {
        callback(it)
    }
}

inline fun CompoundButton.onCheck(crossinline callback: (isChecked: Boolean) -> Unit) {
    setOnCheckedChangeListener { _, isChecked ->
        callback(isChecked)
    }
}

inline fun TextView.afterChange(crossinline callback: (s: String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable) {
            callback(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}

inline fun TextView.beforeChange(crossinline callback: (s: String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable) {}

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            callback(s.toString())
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}

inline fun TextView.onChange(crossinline callback: (s: String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable) {}

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            callback(s.toString())
        }
    })
}


fun Any.md5(): String {
    val st = this.toString()
    var digest = ByteArray(0)

    try {
        val messageDigest = MessageDigest.getInstance("MD5")
        messageDigest.reset()
        messageDigest.update(st.toByteArray())
        digest = messageDigest.digest()
    } catch (ignore: NoSuchAlgorithmException) {
    }

    var md5Hex = BigInteger(1, digest).toString(16)

    while (md5Hex.length < 32) {
        md5Hex = "0$md5Hex"
    }

    return md5Hex
}

fun ViewGroup.inflate(layoutId: Int, root: ViewGroup = this, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(this.context).inflate(layoutId, root, attachToRoot)
}

fun View.inflate(layoutId: Int): View {
    return this.context.inflate(layoutId)
}

fun Context.inflate(layoutId: Int): View {
    return LayoutInflater.from(this).inflate(layoutId, null, false)
}

fun showTimePicker(context: Context, theme: Int = 0, calendar: Calendar, callback: (calendar: Calendar) -> Unit) {
    val timePickerDialog = if (theme == 0) TimePickerDialog(
            context,
            TimePickerDialog.OnTimeSetListener { _, h, m ->
                val cal = GregorianCalendar()
                cal.timeInMillis = calendar.timeInMillis
                cal.set(Calendar.HOUR_OF_DAY, h)
                cal.set(Calendar.MINUTE, m)
                callback(cal)
            },
            calendar.get(Calendar.HOUR_OF_DAY),
            calendar.get(Calendar.MINUTE),
            true
    ) else TimePickerDialog(
            context,
            theme,
            TimePickerDialog.OnTimeSetListener { _, h, m ->
                val cal = GregorianCalendar()
                cal.timeInMillis = calendar.timeInMillis
                cal.set(Calendar.HOUR_OF_DAY, h)
                cal.set(Calendar.MINUTE, m)
                callback(cal)
            },
            calendar.get(Calendar.HOUR_OF_DAY),
            calendar.get(Calendar.MINUTE),
            true
    )
    timePickerDialog.show()
}

fun showDatePicker(context: Context, theme: Int = 0, calendar: Calendar = Calendar.getInstance(), minDate: Long = 0L, callback: (calendar: Calendar) -> Unit) {
    val datePickerDialog = if (theme == 0) DatePickerDialog(
            context,
            DatePickerDialog.OnDateSetListener { _, y, m, d ->
                val cal = GregorianCalendar()
                cal.timeInMillis = calendar.timeInMillis
                cal.set(y, m, d)
                callback(cal)
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
    ) else DatePickerDialog(
            context,
            theme,
            DatePickerDialog.OnDateSetListener { _, y, m, d ->
                val cal = GregorianCalendar()
                cal.timeInMillis = calendar.timeInMillis
                cal.set(y, m, d)
                callback(cal)
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
    )

    if (minDate != 0L) {
        datePickerDialog.datePicker.minDate = minDate
    }

    datePickerDialog.show()
}


fun Context.getColorValue(resId: Int): Int {
    return resources.getColor(resId)
}

fun View.getColorValue(resId: Int): Int {
    return resources.getColor(resId)
}

fun View.getDimen(resource: Int): Int {
    return context.resources.getDimensionPixelSize(resource)
}

fun View.getStatusBarHeight(): Int {
    var result = 0
    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    if (resourceId > 0) {
        result = resources.getDimensionPixelSize(resourceId)
    }
    return result
}

fun View.printTouchEvents() {
    setOnTouchListener { v, event ->
        when (event.action) {
            MotionEvent.ACTION_MASK -> log("${v::class.java.simpleName}\tACTION_MASK")
            MotionEvent.ACTION_DOWN -> log("${v::class.java.simpleName}\tACTION_DOWN")
            MotionEvent.ACTION_UP -> log("${v::class.java.simpleName}\tACTION_UP")
            MotionEvent.ACTION_MOVE -> log("${v::class.java.simpleName}\tACTION_MOVE")
            MotionEvent.ACTION_CANCEL -> log("${v::class.java.simpleName}\tACTION_CANCEL")
            MotionEvent.ACTION_OUTSIDE -> log("${v::class.java.simpleName}\tACTION_OUTSIDE")
            MotionEvent.ACTION_POINTER_DOWN -> log("${v::class.java.simpleName}\tACTION_POINTER_DOWN")
            MotionEvent.ACTION_POINTER_UP -> log("${v::class.java.simpleName}\tACTION_POINTER_UP")
            MotionEvent.ACTION_HOVER_MOVE -> log("${v::class.java.simpleName}\tACTION_HOVER_MOVE")
            MotionEvent.ACTION_SCROLL -> log("${v::class.java.simpleName}\tACTION_SCROLL")
            MotionEvent.ACTION_HOVER_ENTER -> log("${v::class.java.simpleName}\tACTION_HOVER_ENTER")
            MotionEvent.ACTION_HOVER_EXIT -> log("${v::class.java.simpleName}\tACTION_HOVER_EXIT")
            MotionEvent.ACTION_BUTTON_PRESS -> log("${v::class.java.simpleName}\tACTION_BUTTON_PRESS")
            MotionEvent.ACTION_BUTTON_RELEASE -> log("${v::class.java.simpleName}\tACTION_BUTTON_RELEASE")
            MotionEvent.ACTION_POINTER_INDEX_MASK -> log("${v::class.java.simpleName}\tACTION_POINTER_INDEX_MASK")
            MotionEvent.ACTION_POINTER_2_DOWN -> log("${v::class.java.simpleName}\tACTION_POINTER_2_DOWN")
            MotionEvent.ACTION_POINTER_3_DOWN -> log("${v::class.java.simpleName}\tACTION_POINTER_3_DOWN")
            MotionEvent.ACTION_POINTER_2_UP -> log("${v::class.java.simpleName}\tACTION_POINTER_2_UP")
            MotionEvent.ACTION_POINTER_3_UP -> log("${v::class.java.simpleName}\tACTION_POINTER_3_UP")
        }
        false
    }
}

fun Context.createTypeface(path: String): Typeface {
    return Typeface.createFromAsset(assets, path)
}

fun Activity.hideKeyboard() {
    val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    //Find the currently focused view, so we can grab the correct window token from it.
    var view = currentFocus
    //If no view currently has focus, with a new one, just so we can grab a window token from it
    if (view == null) {
        view = View(this)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun ValueAnimator.onEnd(call: () -> Unit) {
    addListener(object : Animator.AnimatorListener {
        override fun onAnimationStart(animation: Animator?) {

        }

        override fun onAnimationEnd(animation: Animator?) {
            call()
        }

        override fun onAnimationCancel(animation: Animator?) {}

        override fun onAnimationRepeat(animation: Animator?) {}
    })
}

fun LinearLayout.onTouchClick(click: () -> Unit) {
    setOnClickListener { }
    setOnTouchListener { _, e ->
        if (e.action == MotionEvent.ACTION_UP) click()
        false
    }
}

fun Int.toStringFirstZero(): String {
    return if (this < 10) "0$this" else toString()
}

fun <T> List<T>.toArrayList(): ArrayList<T> {
    if (this is ArrayList) {
        return this
    }
    val list = ArrayList<T>()
    list.addAll(this)
    return list
}

fun <T> List<T>.toMutableList(): MutableList<T> {
    if (this is MutableList) {
        return this
    }
    val list = ArrayList<T>(this.size)
    list.addAll(this)
    return list
}

inline fun <T> MutableList<T>.removeFirst(call: (T) -> Boolean): Int {
    val index = this.indexOfFirst(call)
    if (index >= 0) {
        this.removeAt(index)
    }
    return index
}

fun <T> MutableList<T>.move(from: Int, to: Int) {
    add(to, removeAt(from))
}

fun EditText.setSelectionEnd() {
    setSelection(text.length)
}

fun TextView.setTextColor(resource: Int? = null, color: Int? = null) {
    if (resource != null) {
        setTextColor(context.resources.getColor(resource))
        return
    }

    if (color != null) {
        setTextColor(color)
        return
    }
}

inline fun <T> tryOrNull(call: () -> T): T? {
    return try {
        call()
    } catch (ignore: Exception) {
        null
    }
}

inline fun anyTry(call: () -> Unit) {
    try {
        call()
    } catch (ignore: Exception) {
    }
}

fun anyTry(call: () -> Unit, handle: (Exception) -> Unit) {
    try {
        call()
    } catch (e: Exception) {
        handle.invoke(e)
    }
}

inline fun Int.doTimes(call: (index: Int) -> Unit) {
    for (i in 0 until this) call(i)
}

private val lorem: List<String> by lazy {
    arrayListOf(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ac orci phasellus egestas tellus rutrum. Aliquet porttitor lacus luctus accumsan tortor posuere ac. Vitae et leo duis ut diam. Tellus molestie nunc non blandit massa. Diam quam nulla porttitor massa id. Erat nam at lectus urna duis convallis convallis. Tempus imperdiet nulla malesuada pellentesque elit eget gravida cum. Malesuada proin libero nunc consequat interdum. Tempus imperdiet nulla malesuada pellentesque elit eget gravida cum sociis. Purus sit amet volutpat consequat mauris. Quis ipsum suspendisse ultrices gravida dictum fusce ut placerat orci. Dui sapien eget mi proin sed libero enim sed faucibus. Tellus rutrum tellus pellentesque eu tincidunt tortor aliquam. Laoreet non curabitur gravida arcu ac tortor dignissim.",
            "Dui accumsan sit amet nulla facilisi. Enim tortor at auctor urna nunc id cursus metus aliquam. Proin sagittis nisl rhoncus mattis rhoncus urna neque viverra. Id semper risus in hendrerit gravida rutrum quisque non. Elementum integer enim neque volutpat ac. Placerat in egestas erat imperdiet sed euismod nisi porta lorem. Tellus elementum sagittis vitae et leo duis. Morbi tincidunt ornare massa eget egestas purus viverra accumsan in. Erat imperdiet sed euismod nisi. Montes nascetur ridiculus mus mauris vitae ultricies leo. Eu non diam phasellus vestibulum lorem sed risus ultricies. Neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt. Imperdiet nulla malesuada pellentesque elit eget gravida cum sociis natoque.",
            "At augue eget arcu dictum varius. Neque convallis a cras semper auctor. Mi tempus imperdiet nulla malesuada pellentesque elit eget. Pellentesque elit eget gravida cum sociis natoque penatibus et. Posuere lorem ipsum dolor sit amet. Viverra aliquet eget sit amet tellus. Nunc pulvinar sapien et ligula ullamcorper malesuada proin libero. At elementum eu facilisis sed odio morbi quis commodo odio. Lobortis feugiat vivamus at augue eget arcu dictum varius duis. Auctor elit sed vulputate mi sit amet mauris commodo. Nunc vel risus commodo viverra maecenas accumsan lacus vel. Habitant morbi tristique senectus et netus et malesuada fames. Ipsum consequat nisl vel pretium lectus quam id leo.",
            "Ornare arcu odio ut sem nulla. Ultrices in iaculis nunc sed augue lacus viverra vitae. Iaculis at erat pellentesque adipiscing commodo elit at. Scelerisque purus semper eget duis at tellus at. Sagittis eu volutpat odio facilisis mauris. Nec feugiat in fermentum posuere urna nec. Amet aliquam id diam maecenas ultricies mi eget mauris. Vitae proin sagittis nisl rhoncus mattis rhoncus urna neque. Donec enim diam vulputate ut pharetra sit. Platea dictumst vestibulum rhoncus est pellentesque. Nisl vel pretium lectus quam id leo in vitae. Et netus et malesuada fames ac turpis egestas. Elementum facilisis leo vel fringilla est. Diam in arcu cursus euismod. Turpis nunc eget lorem dolor sed viverra ipsum nunc aliquet. Sagittis orci a scelerisque purus semper.",
            "Volutpat odio facilisis mauris sit amet massa vitae. Consectetur adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Pharetra massa massa ultricies mi. Vitae nunc sed velit dignissim sodales. Nec ultrices dui sapien eget mi. Id neque aliquam vestibulum morbi blandit cursus risus at ultrices. Hendrerit dolor magna eget est lorem ipsum. Cras semper auctor neque vitae tempus quam. Nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi. Quis risus sed vulputate odio. Hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Nisl purus in mollis nunc sed id semper risus in. Libero id faucibus nisl tincidunt eget nullam. Volutpat lacus laoreet non curabitur gravida. Felis imperdiet proin fermentum leo vel orci porta non pulvinar. Tristique senectus et netus et malesuada fames ac turpis egestas. Ultrices in iaculis nunc sed augue lacus viverra."
    )
}

fun randomBoolean(): Boolean {
    return Random().nextBoolean()
}

fun randomLong(min: Long = System.currentTimeMillis() - dayInMillis, max: Long = System.currentTimeMillis() + dayInMillis): Long {
    return (((max - min) * randomInt(max = 100)) / 100) + min
}

fun randomInt(min: Int = 0, max: Int = 100): Int {
    return Random().nextInt(max - min + 1) + min
}

fun Long.Companion.random(min: Long = System.currentTimeMillis() - dayInMillis, max: Long = System.currentTimeMillis() + dayInMillis): Long {
    return (((max - min) * randomInt(max = 100)) / 100) + min
}

fun Int.Companion.random(min: Int = 0, max: Int = 100): Int {
    return Random().nextInt(max - min + 1) + min
}

fun Boolean.random(): Boolean {
    return Random().nextBoolean()
}

fun <T> List<T>.random(): T? {
    if (isEmpty()) return null
    return get(Int.random(min = 0, max = lastIndex))
}

fun randomTextLine(length: Int = 200): String {
    var string = lorem[randomInt(0, lorem.lastIndex)]
    return when {
        string.length == length -> string
        string.length > length -> string.substring(0, length - 1)
        else -> {
            while (string.length < length) {
                string += string
            }

            if (string.length > length) string.substring(0, length - 1)
            else string
        }
    }
}

fun randomText(lines: Int = 1, linesLength: Int = 200): String {
    val builder = StringBuilder()
    lines.doTimes {
        if (it != 0)
            builder.append("\n")
        builder.append(randomTextLine(linesLength))
    }
    return builder.toString()
}

fun <T, R> List<T>.mapByPair(transform: (first: T, second: T) -> R): ArrayList<R> {
    if (isEmpty()) return arrayListOf()

    var first: T = this[0]

    val result = ArrayList<R>()
    forEachIndexed { index, value ->
        if (index % 2 == 0) {
            first = value
        } else {
            result.add(transform(first, value))
        }
    }

    return result
}


fun <T, R> List<T>.mapByCrossedPair(transform: (first: T, second: T) -> R): ArrayList<R> {
    if (isEmpty()) return arrayListOf()

    val result = ArrayList<R>()

    forEachIndexed { index, _ ->
        if (index != lastIndex) {
            result.add(transform(this[index], this[index + 1]))
        }
    }

    return result
}

inline fun <T> Iterable<T>.contains(predicate: (T) -> Boolean): Boolean {
    return firstOrNull(predicate) != null
}


class LoadingDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = ProgressBar(activity)

        if (colorId != defaultColor && colorId >= 0) {
            val color = view.resources.getColor(colorId)
            view.indeterminateTintList = ColorStateList.valueOf(color)
        }

        val dialog = AlertDialog.Builder(activity!!)
                .setView(view)
                .create()

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    companion object {
        var colorId: Int = -1
        const val defaultColor: Int = -1
        const val dialogTag = "LoadingDialog"

        fun start(manager: FragmentManager): LoadingDialog {
            val dialog = LoadingDialog()
            dialog.show(manager, dialogTag)
            return dialog
        }
    }
}

/**
 * Create and show not cancelable dialog with round progress bar
 * Dialog have tag "LoadingDialog"
 */
fun Activity.createLoadingDialog(): LoadingDialog {
    val fragment = this.fragmentManager.findFragmentByTag(LoadingDialog.dialogTag)
    if (fragment != null && fragment is LoadingDialog) {
        return fragment
    }
    return LoadingDialog.start(this.fragmentManager)
}

/**
 * Create and show not cancelable dialog with round progress bar
 * Dialog have tag "LoadingDialog"
 */
fun Fragment.createLoadingDialog(): LoadingDialog {
    return activity!!.createLoadingDialog()
}

/**
 * Try find and dismiss dialog with tag "LoadingDialog" witch created by createLoadingDialog()
 */
fun Activity.hideLoadingDialog() {
    anyTry {
        val fragment = this.fragmentManager.findFragmentByTag(LoadingDialog.dialogTag)
        if (fragment != null && fragment is LoadingDialog) {
            fragment.dismiss()
        }
    }
}

/**
 * Try find and dismiss dialog with tag "LoadingDialog" witch created by createLoadingDialog()
 */
fun Fragment.hideLoadingDialog() {
    activity?.hideLoadingDialog()
}


/**
 * Try find dialog with tag "LoadingDialog" witch created by createLoadingDialog()
 */
fun Activity.isLoadingDialogExists(): Boolean {
    return tryOrNull {
        this.fragmentManager.findFragmentByTag(LoadingDialog.dialogTag)
    } != null
}

/**
 * Try find dialog with tag "LoadingDialog" witch created by createLoadingDialog()
 */
fun Fragment.isLoadingDialogExists(): Boolean {
    return activity!!.isLoadingDialogExists()
}


fun <T : RecyclerView.ViewHolder> RecyclerView.init(
        adapter: RecyclerView.Adapter<T>? = null,
        layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this.context),
        itemAnimator: RecyclerView.ItemAnimator? = DefaultItemAnimator()
) {

    if (adapter != null) {
        this.adapter = adapter
    }

    this.layoutManager = layoutManager

    this.itemAnimator = itemAnimator

}

fun Fragment.getColor(res: Int): Int {
    return resources.getColor(res)
}

fun Fragment.getDrawable(res: Int): Drawable {
    return resources.getDrawable(res)
}

fun View.showPopupMenu(menuRes: Int, theme: Int? = null, call: (MenuItem) -> Boolean) {
    val wrapper = if (theme != null) ContextThemeWrapper(context, theme) else ContextThemeWrapper()
    val menu = PopupMenu(wrapper, this)
    menu.inflate(menuRes)
    menu.setOnMenuItemClickListener(call)
    menu.show()
}

fun postDelayed(delay: Long = 500L, call: () -> Unit) {
    Handler(Looper.getMainLooper()).postDelayed({
        call()
    }, delay)
}

fun getStringByNumberCase(
        number: Int,
        caseOne: Int,
        caseTwo: Int,
        caseMany: Int
): Int {
    val string = number.toString()
    return when {
        string.endsWith("11") || string.endsWith("12") || string.endsWith("13") || string.endsWith("14") -> caseMany
        string.endsWith("1") -> caseOne
        string.endsWith("2") || string.endsWith("3") || string.endsWith("4") -> caseTwo
        else -> caseMany
    }
}