package basilliyc.extensions

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerHolder<T>(val view: View) : RecyclerView.ViewHolder(view) {
    abstract fun onBind(item: T)
}

abstract class BaseRecyclerAdapter<T>(
        var data: ArrayList<T> = ArrayList(),
        var onClick: ((T) -> Unit)? = null,
        var onLongClick: ((T) -> Unit)? = null,
        var onClickView: ((T, View) -> Unit)? = null,
        var onLongClickView: ((T, View) -> Unit)? = null
) : RecyclerView.Adapter<BaseRecyclerHolder<T>>() {

    private var valuesInited = false

    open fun getLayout(type: Int): Int {
        return 0
    }

    abstract fun onBindHolder(holder: BaseRecyclerHolder<T>, item: T)

    open fun setData(list: List<T>, animate: Boolean = false, clear: Boolean = true): BaseRecyclerAdapter<T> {
        if (animate)
            notifyItemRangeRemoved(0, itemCount)
        if (clear)
            data.clear()
        data.addAll(list)
        if (animate) notifyItemRangeInserted(0, itemCount)
        else notifyDataSetChanged()
        return this
    }

    fun onClick(call: (T) -> Unit): BaseRecyclerAdapter<T> {
        onClick = call
        return this
    }

    fun onLongClick(call: (T) -> Unit): BaseRecyclerAdapter<T> {
        onLongClick = call
        return this
    }

    fun onClickView(call: (T, View) -> Unit): BaseRecyclerAdapter<T> {
        onClickView = call
        return this
    }

    fun onLongClickView(call: (T, View) -> Unit): BaseRecyclerAdapter<T> {
        onLongClickView = call
        return this
    }

    open fun onInitValues(context: Context) {}

    open fun onBindFirst(holder: BaseRecyclerHolder<T>, item: T) {}

    open fun onBindMiddle(holder: BaseRecyclerHolder<T>, item: T) {}

    open fun onBindLast(holder: BaseRecyclerHolder<T>, item: T) {}

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): BaseRecyclerHolder<T> {
        return object : BaseRecyclerHolder<T>(parent.inflate(getLayout(type))) {
            override fun onBind(item: T) {
                onBindHolder(this, item)
            }
        }
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: BaseRecyclerHolder<T>, position: Int) {
        if (!valuesInited) {
            onInitValues(holder.view.context)
            valuesInited = true
        }

        val item = data[position]
        when (position) {
            0 -> onBindFirst(holder, item)
            data.lastIndex -> onBindLast(holder, item)
            else -> onBindMiddle(holder, item)
        }
        holder.onBind(item)
        if (onClick != null) {
            holder.view.setOnClickListener {
                onClick?.invoke(item)
            }
        } else if (onClickView != null) {
            holder.view.setOnClickListener {
                onClickView?.invoke(item, holder.view)
            }
        }

        if (onLongClick != null) {
            holder.view.setOnLongClickListener {
                onLongClick?.invoke(item)
                true
            }
        } else if (onLongClickView != null) {
            holder.view.setOnLongClickListener {
                onLongClickView?.invoke(item, holder.view)
                true
            }
        }
    }
}